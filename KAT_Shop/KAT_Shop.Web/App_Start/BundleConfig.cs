﻿using System.Web;
using System.Web.Optimization;

namespace KAT_Shop.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));
                                    
          
            //Scripts Shared

            const string ANGULAR_APP_ROOT_SHARED = "~/app/shared/";
            const string VIRTUAL_BUNDLE_PATH_SHARED = ANGULAR_APP_ROOT_SHARED + "js/shared.js";
            var scriptBundle_Shared = new ScriptBundle(VIRTUAL_BUNDLE_PATH_SHARED).Include(
                    ANGULAR_APP_ROOT_SHARED + "modules/katshop.common.js",
                    ANGULAR_APP_ROOT_SHARED + "filter/isPublishFilter.js",
                    ANGULAR_APP_ROOT_SHARED + "filter/isStatusOrderFilter.js",
                    ANGULAR_APP_ROOT_SHARED + "services/apiService.js",
                    ANGULAR_APP_ROOT_SHARED + "directives/pagerDirective.js",
                    ANGULAR_APP_ROOT_SHARED + "directives/asDateDirective.js",
                    ANGULAR_APP_ROOT_SHARED + "services/notificationService.js",
                    ANGULAR_APP_ROOT_SHARED + "services/authData.js",
                    ANGULAR_APP_ROOT_SHARED + "services/authenticationService.js",
                    ANGULAR_APP_ROOT_SHARED + "services/loginService.js",
                    ANGULAR_APP_ROOT_SHARED + "services/commonService.js"
                    );
            bundles.Add(scriptBundle_Shared);

            //Scripts Module

            const string ANGULAR_APP_ROOT_APP = "~/app/components/";

            const string VIRTUAL_BUNDLE_PATH_MODULE = ANGULAR_APP_ROOT_APP + "js/module.js";
            var scriptBundle_Module = new ScriptBundle(VIRTUAL_BUNDLE_PATH_MODULE)
                .IncludeDirectory(ANGULAR_APP_ROOT_APP, "*.module.js", searchSubdirectories: true);
            bundles.Add(scriptBundle_Module);


            //Scripts controller                                           
            const string VIRTUAL_BUNDLE_PATH_CONTROLLER = ANGULAR_APP_ROOT_APP + "js/controller.js";
            var scriptBundle_Controller = new ScriptBundle(VIRTUAL_BUNDLE_PATH_CONTROLLER)
                .IncludeDirectory(ANGULAR_APP_ROOT_APP, "*Controller.js", searchSubdirectories: true);
            bundles.Add(scriptBundle_Controller);

        }
    }
}
