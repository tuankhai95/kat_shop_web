﻿/// <reference path="/Assets/admin/libs/angular/angular.js" />

(function () {
    angular.module('katshop',
            [
             'katshop.application_groups',
             'katshop.application_roles',
             'katshop.application_users',
             'katshop.product_categories',
             'katshop.product_brands',
             'katshop.post_categories',
             'katshop.product_attribute_groups',
             'katshop.product_attributes',
             'katshop.posts',
             'katshop.product_templates',
             'katshop.products',
             'katshop.orders',
             'katshop.feedbacks',
             'katshop.statistics',
             'katshop.common'
            ])
        .config(config)
    .config(configAuthentication);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    function config($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('base', {
                url: '',
                templateUrl: 'app/shared/views/baseView.html',
                abstract: true
            })
            .state('login', {
                url: '/login',
                templateUrl: "/app/components/login/loginView.html",
                controller: "loginController"
            })
            .state('home', {
                url: '/admin',
                parent: 'base',
                templateUrl: "/app/components/home/homeView.html",
                controller: "homeController"
            })
            .state('404notfound', {
                url: '/404notfound.html',
                templateUrl: "/app/components/home/404NotFoundView.html"
            });

        $urlRouterProvider.otherwise('/404notfound.html');
    }

    function configAuthentication($httpProvider) {
        $httpProvider.interceptors.push(function ($q, $location) {
            return {
                request: function (config) {

                    return config;
                },
                requestError: function (rejection) {

                    return $q.reject(rejection);
                },
                response: function (response) {
                    if (response.status == "401") {
                        $location.path('/login');
                    }
                    return response;
                },
                responseError: function (rejection) {

                    if (rejection.status == "401") {
                        $location.path('/login');
                    }
                    return $q.reject(rejection);
                }
            };
        });
    }
})();