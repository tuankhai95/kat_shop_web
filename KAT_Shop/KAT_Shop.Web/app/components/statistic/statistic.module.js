﻿/// <reference path="/Assets/admin/libs/angular/angular.js" />

(function () {
    angular.module('katshop.statistics', ['katshop.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('statistic_revenue', {
                url: "/statistic_revenue",
                parent: 'base',
                templateUrl: "/app/components/statistic/revenueStatisticView.html",
                controller: "revenueStatisticController"
            }).state('statistic_product', {
                url: "/statistic_product",
                parent: 'base',
                templateUrl: "/app/components/statistic/productStatisticView.html",
                controller: "productStatisticController"
            });
    }
})();