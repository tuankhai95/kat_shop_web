﻿(function (app) {
    'use strict';

    app.controller('productAttributeAddController', productAttributeAddController);

    productAttributeAddController.$inject = ['$scope', 'apiService', 'notificationService', '$location', 'commonService'];

    function productAttributeAddController($scope, apiService, notificationService, $location, commonService) {
        $scope.productAttribute = {
            Id: 0
        }

        $scope.addProductAttribute = addProductAttribute;

        function addProductAttribute() {
            apiService.post('/api/productattribute/create', $scope.productAttribute, addSuccessed, addFailed);
        }

        function addSuccessed() {
            notificationService.displaySuccess($scope.productAttribute.Name + ' đã được thêm mới.');

            $location.url('product_attributes');
        }
        function addFailed(response) {
            notificationService.displayError('Thêm mới không thành công!');
            notificationService.displayErrorValidation(response);
        }

        function loadProductAttributeGroups() {   
            apiService.get('/api/productattributegroup/getallparents', null,
                function (result) {
                    $scope.productAttributeGroups = result.data;
                }, function () {
                    notificationService.displayError('Không tải được danh sách quyền.');
                });  
        }

        $scope.resetForm = function resetForm() {
            $scope.productAttribute = {};
        }
        loadProductAttributeGroups();
    }
})(angular.module('katshop.product_attributes'));