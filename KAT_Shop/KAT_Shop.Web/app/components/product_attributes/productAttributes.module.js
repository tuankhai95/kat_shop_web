﻿/// <reference path="/Assets/admin/libs/angular/angular.js" />

(function () {
    angular.module('katshop.product_attributes', ['katshop.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('product_attributes', {
            url: "/product_attributes",
            templateUrl: "/app/components/product_attributes/productAttributeListView.html",
            parent: 'base',
            controller: "productAttributeListController"
        })
            .state('add_product_attribute', {
                url: "/add_product_attribute",
                parent: 'base',
                templateUrl: "/app/components/product_attributes/productAttributeAddView.html",
                controller: "productAttributeAddController"
            })
            .state('edit_product_attribute', {
                url: "/edit_product_attribute/:id",
                templateUrl: "/app/components/product_attributes/productAttributeEditView.html",
                controller: "productAttributeEditController",
                parent: 'base',
            });
    }
})();