﻿(function (app) {
    'use strict';

    app.controller('productAttributeEditController', productAttributeEditController);

    productAttributeEditController.$inject = ['$scope', 'apiService', 'notificationService', '$location', '$stateParams'];

    function productAttributeEditController($scope, apiService, notificationService, $location, $stateParams) {
        $scope.productAttribute = {}
                                   
        $scope.updateProductAttribute = updateProductAttribute;

        function updateProductAttribute() {
            apiService.put('/api/productattribute/update', $scope.productAttribute, addSuccessed, addFailed);
        }
        function loadDetail() {
            apiService.get('/api/productattribute/getbyid/' + $stateParams.id, null,
            function (result) {
                $scope.productAttribute = result.data;
            },
            function (result) {
                notificationService.displayError(result.data);
            });
        }

        function addSuccessed() {
            notificationService.displaySuccess($scope.productAttribute.Name + ' đã được cập nhật thành công.');

            $location.url('product_attributes');
        }
        function addFailed(response) {
            notificationService.displayError('Cập nhật không thành công.');
            notificationService.displayErrorValidation(response);
        }
        $scope.resetForm = function resetForm() {
            loadDetail();
        }
                          
        function loadProductAttributeGroups() {
            apiService.get('/api/productattributegroup/getallparents', null,
                function (result) {
                    $scope.productAttributeGroups = result.data;
                }, function () {
                    notificationService.displayError('Không tải được danh sách quyền.');
                });
        }
        loadProductAttributeGroups();
        loadDetail();
    }
})(angular.module('katshop.application_roles'));