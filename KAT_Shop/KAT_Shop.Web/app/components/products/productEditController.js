﻿(function (app) {
    app.controller('productEditController', productEditController)

    productEditController.$inject = ['apiService', '$scope', 'notificationService', '$state', 'commonService', '$stateParams'];
    function productEditController(apiService, $scope, notificationService, $state, commonService, $stateParams) {
        $scope.product = {
            CreatedDate: new Date(),
            IsPublished: true,
            ViewCount: 0,
            IsDelete: false,
            AttributeValues: []
        }

        $scope.ProductAttributes = [];
        $scope.addingAttribute = null;
        $scope.moreImages = [];
        $scope.UpdateProduct = UpdateProduct;
        $scope.GetSeoTitle = GetSeoTitle;  
        $scope.flatFolders = [];
        $scope.productCategories = {};

        //Gọi CKEditor
        $scope.ckeditorOptions = {
            languague: 'vi',
            height: '250px',
        }

        //Chuyển đổi ký tự thành không dấu
        function GetSeoTitle() {
            $scope.product.Alias = commonService.getSeoTitle($scope.product.Name);
            $scope.product.NormalizedName = commonService.getNormalizedName($scope.product.Name);
        }
       
        function loadProductDetail() {
            apiService.get('api/product/getproductbyid/' + $stateParams.id, null, function (result) {
                var i, index, attributeIds;
                $scope.product = result.data;
                $scope.moreImages = JSON.parse($scope.product.MoreImages);

                attributeIds = $scope.ProductAttributes.map(function (item) { return item.ID; });
                for (i = 0; i < $scope.product.AttributeValues.length; i = i + 1) {
                    index = attributeIds.indexOf($scope.product.AttributeValues[i].Attribute.ID);
                    attributeIds.splice(index, 1);
                    $scope.ProductAttributes.splice(index, 1);
                }
            }, function (error) {
                notificationService.displayError(error.data);
            });
        }

        function UpdateProduct() {
            $scope.product.MoreImages = JSON.stringify($scope.moreImages);
            apiService.put('api/product/update', $scope.product, function (result) {
                notificationService.displaySuccess(result.data.Name + ' đã được cập nhật.');
                $state.go('products');
            }, function (error) {
                notificationService.displayError('Cập nhật không thành công!');
                notificationService.displayErrorValidation(response);
            });
        }

        function loadProductCategory() {
            apiService.get('api/productcategory/getallparents', null, function (result) {
                $scope.productCategories = commonService.getTree(result.data, 'ID', 'ParentID');
                $scope.productCategories.forEach(function (item) {
                    commonService.recur(item, 0, $scope.flatFolders);
                });
            }, function () {
                console.log('Tải danh mục không thành công!');
            });
        }

        function loadProductBrand() {
            apiService.get('api/productbrand/getallparents', null, function (result) {
                $scope.productBrands = result.data;
            }, function () {
                console.log('Tải danh sách nhãn hiệu thất bại!');
            });
        }

        function loadProductTemplate() {
            apiService.get('api/producttemplate/getallparents', null, function (result) {
                $scope.productTemplates = result.data;
            }, function () {
                console.log('Tải danh sách nhãn hiệu thất bại!');
            });
        }

        function getProductAttributes() {
            apiService.get('api/productattribute/getallparents', null, function (result) {
                $scope.ProductAttributes = result.data;
            }, function () {
                console.log('Tải dữ liệu không thành công!');
            });
        }

        $scope.applyTemplate = function applyTemplate() {
            var template, i, index, workingAttr, attributeValue,
                nonTemplateAttrs = [];

            apiService.get('/api/producttemplate/getbyid/' + $scope.product.template.ID, null, function (result) {
                template = result.data;
                for (i = 0; i < template.ProductAttributes.length; i = i + 1) {
                    workingAttr = $scope.product.AttributeValues.find(function (item) { return item && item.AttributeId === template.ProductAttributes[i].ID; });
                    if (workingAttr) {
                        continue;
                    }
                    workingAttr = $scope.ProductAttributes.find(function (item) { return item && item.ID === template.ProductAttributes[i].ID; }); 
                    index = $scope.ProductAttributes.indexOf(workingAttr);
                    $scope.ProductAttributes.splice(index, 1);
                    attributeValue = {
                        AttributeId: workingAttr.ID,
                        Attribute: workingAttr,
                        ProductId: $scope.product.ID,
                        Value: null
                    };
                    $scope.product.AttributeValues.push(attributeValue);
                }

                for (i = 0; i < $scope.product.AttributeValues.length; i = i + 1) {
                    workingAttr = template.ProductAttributes.find(function (item) { return item && item.ID === $scope.product.AttributeValues[i].AttributeId; });
                    if (!workingAttr) {
                        nonTemplateAttrs.push($scope.product.AttributeValues[i]);
                    }
                }

                for (i = 0; i < nonTemplateAttrs.length; i = i + 1) {
                    workingAttr = $scope.product.AttributeValues.find(function (item) { return item && item.ID === nonTemplateAttrs[i].ID; });
                    index = $scope.product.AttributeValues.indexOf(workingAttr);
                    $scope.product.AttributeValues.splice(index, 1);
                    $scope.ProductAttributes.push(workingAttr.Attribute);
                }
            }, function () {
                console.log('Tải không thành công!');
            })

        };

        $scope.addAttribute = function addAttribute(attr) {
            var index = $scope.ProductAttributes.indexOf(attr);              
            $scope.ProductAttributes.splice(index, 1);
            var attributeValue = {
                AttributeId: attr.ID,
                Attribute: attr,
                ProductId: $scope.product.ID,                
                Value: null
            };
            $scope.product.AttributeValues.push(attributeValue);
            $scope.addingAttribute = null;
        };

        $scope.removeAttribute = function removeAttribute(attr) {
            var index = $scope.product.AttributeValues.indexOf(attr);
            $scope.product.AttributeValues.splice(index, 1);
            $scope.ProductAttributes.push(attr.Attribute);
        };

        $scope.ChooseImage = function () {
            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $scope.$apply(function () {
                    $scope.product.Image = fileUrl;
                })
            }
            finder.popup();
        }

        $scope.ChooseMoreImage = function () {
            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $scope.$apply(function () {
                    $scope.moreImages.push(fileUrl);
                })

            }
            finder.popup();
        }

        $scope.deleteItem = function (index) {
            $scope.moreImages.splice(index, 1);
        }

        loadProductCategory();
        loadProductBrand();
        loadProductTemplate();
        getProductAttributes();
        loadProductDetail();
    }


})(angular.module('katshop.products'));