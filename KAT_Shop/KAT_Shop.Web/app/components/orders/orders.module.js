﻿/// <reference path="/Assets/admin/libs/angular/angular.js" />

(function () {
    angular.module('katshop.orders', ['katshop.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('orders', {
                url: "/orders",
                parent:'base',
                templateUrl: "/app/components/orders/orderListView.html",
                controller: "orderListController"
            }).state('order_edit', {
                url: "/order_edit/:id",
                parent: 'base',
                templateUrl: "/app/components/orders/orderEditView.html",
                controller: "orderEditController"
            }).state('order_print', {
                url: "/order_print/:id",
                templateUrl: "/app/components/orders/orderPrintView.html",
                controller: "orderPrintController",
                reload:true
            });
    }
})();