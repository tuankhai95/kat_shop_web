﻿/// <reference path="/Assets/admin/libs/angular/angular.js" />

(function () {
    angular.module('katshop.product_brands', ['katshop.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('product_brands', {
            url: "/product_brands",
            parent: 'base',
            templateUrl: "/app/components/product_brands/productBrandListView.html",
            controller: "productBrandListController"
        }).state('add_product_brand', {
            url: "/add_product_brand",
            parent: 'base',
            templateUrl: "/app/components/product_brands/productBrandAddView.html",
            controller: "productBrandAddController"
        }).state('edit_product_brand', {
            url: "/edit_product_brand/:id",
            parent: 'base',
            templateUrl: "/app/components/product_brands/productBrandEditView.html",
            controller: "productBrandEditController"
        });
    }
})();