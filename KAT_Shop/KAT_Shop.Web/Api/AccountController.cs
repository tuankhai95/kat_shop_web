﻿using Microsoft.AspNet.Identity.Owin;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using KAT_Shop.Web.App_Start;
using KAT_Shop.Web.Models;
using KAT_Shop.Model.Models;
using System;
using KAT_Shop.Service;
using KAT_Shop.Common.Exceptions;
using System.Web.Script.Serialization;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private IGroupService _appGroupService;
        private IRoleService _appRoleService;
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IGroupService appGroupService, IRoleService appRoleService)

        {
            UserManager = userManager;
            SignInManager = signInManager;
            _appGroupService = appGroupService;
            _appRoleService = appRoleService;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<HttpResponseMessage> Login(HttpRequestMessage request, string userName, string password, bool rememberMe = false)
        {
            if (!ModelState.IsValid)
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true

            var result = await SignInManager.PasswordSignInAsync(userName, password, rememberMe, shouldLockout: false);
            if (result == 0)
            {
                var user = await _userManager.FindByNameAsync(userName);
                return request.CreateResponse(HttpStatusCode.OK, user);
            }
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> Register(HttpRequestMessage request, string registerViewModel)
        {
            var model = new JavaScriptSerializer().Deserialize<RegisterViewModel>(registerViewModel);
            Validate<RegisterViewModel>(model);
            if (ModelState.IsValid)
            {
                var userByUserName = await _userManager.FindByNameAsync(model.UserName);
                if (userByUserName != null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Tên tài khoản đã tồn tại!");
                }
                var user = new User()
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    BirthDay = model.BirthDay,
                    FullName = model.FullName,
                    PhoneNumber = model.PhoneNumber,
                    Address = model.Address
                };
                try
                {
                    user.Id = Guid.NewGuid().ToString();
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var userGroup = new UserGroup();
                        userGroup.GroupId = _appGroupService.GetByGroupName(model.Groups).ID;
                        userGroup.UserId = user.Id;
                        //add role to user
                        var listRole = _appRoleService.GetListRoleByGroupId(userGroup.GroupId);
                        foreach (var role in listRole)
                        {
                            await _userManager.RemoveFromRoleAsync(user.Id, role.Name);
                            await _userManager.AddToRoleAsync(user.Id, role.Name);
                        }

                        _appGroupService.AddUserToGroups(userGroup, user.Id);
                        _appGroupService.Save();
                        model.ID = user.Id;
                        return request.CreateResponse(HttpStatusCode.OK, model);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                    }
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

    }
}
