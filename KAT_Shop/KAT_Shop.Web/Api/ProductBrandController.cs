﻿using AutoMapper;
using KAT_Shop.Model.Models;
using KAT_Shop.Service;
using KAT_Shop.Web.Infrastruture.Core;
using KAT_Shop.Web.Infrastruture.Extensions;
using KAT_Shop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/productbrand")]
    [Authorize]
    public class ProductBrandController : ApiControllerBase
    {
        private IProductBrandService _productBrandService;
        private IProductService _productService;

        public ProductBrandController(IErrorService errorService, IProductBrandService productBrandService, IProductService productService) : base(errorService)
        {
            _productBrandService = productBrandService;
            _productService = productService;
        }

        [Route("getall")]
        [HttpGet]
        [Authorize(Roles = "Access")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, string keyword, int page, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _productBrandService.GetAll(keyword);

                totalRow = model.Count();
                var query = (pageSize == -1) ? model.OrderBy(x => x.Name) : model.OrderBy(x => x.Name).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<ProductBrand>, IEnumerable<ProductBrandViewModel>>(query);

                var paginationSet = new PaginationSet<ProductBrandViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (pageSize == -1) ? 1 : (int)Math.Ceiling((decimal)totalRow / pageSize)
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)

        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productBrandService.GetById(id);

                var responseData = Mapper.Map<ProductBrand, ProductBrandViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getallparents")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productBrandService.GetAll().OrderBy(x => x.Name);

                var responseData = Mapper.Map<IEnumerable<ProductBrand>, IEnumerable<ProductBrandViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("create")]
        [HttpPost]
        [Authorize(Roles = "Add")]
        public HttpResponseMessage Create(HttpRequestMessage request, ProductBrandViewModel productBrandVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newProductBrand = new ProductBrand();
                    newProductBrand.UpdateProductBrand(productBrandVm);
                    newProductBrand.CreatedDate = DateTime.Now;
                    newProductBrand.CreatedBy = User.Identity.Name;
                    _productBrandService.Add(newProductBrand);
                    _productBrandService.Save();

                    var responseData = Mapper.Map<ProductBrand, ProductBrandViewModel>(newProductBrand);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Authorize(Roles = "Edit")]
        public HttpResponseMessage Update(HttpRequestMessage request, ProductBrandViewModel productBrandVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbProductBrand = _productBrandService.GetById(productBrandVm.ID);

                    dbProductBrand.UpdateProductBrand(productBrandVm);
                    dbProductBrand.UpdatedDate = DateTime.Now;
                    dbProductBrand.UpdatedBy = User.Identity.Name;
                    _productBrandService.Update(dbProductBrand);
                    _productBrandService.Save();

                    var responseData = Mapper.Map<ProductBrand, ProductBrandViewModel>(dbProductBrand);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listProduct = _productService.GetListProductByCategoryId(id);
                    if (listProduct.Count() > 0)
                    {
                        foreach (var product in listProduct)
                        {
                            _productService.Delete(product.ID);
                        }
                    }
                    var oldProductBrand = _productBrandService.Delete(id);
                    _productBrandService.Save();

                    var responseData = Mapper.Map<ProductBrand, ProductBrandViewModel>(oldProductBrand);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductBranbs)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listProductBrand = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductBranbs);
                    var oldProductBrand = new List<ProductBrand>();
                    foreach (var item in listProductBrand)
                    {
                        var listProduct = _productService.GetListProductByCategoryId(item);
                        if (listProduct.Count() > 0)
                        {
                            foreach (var product in listProduct)
                            {
                                _productService.Delete(product.ID);
                            }
                        }
                        oldProductBrand.Add(_productBrandService.Delete(item));
                    }

                    _productBrandService.Save();

                    var responseData = Mapper.Map<IEnumerable<ProductBrand>, IEnumerable<ProductBrandViewModel>>(oldProductBrand);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

    }
}
