﻿using AutoMapper;
using KAT_Shop.Common;
using KAT_Shop.Model.Models;
using KAT_Shop.Service;
using KAT_Shop.Web.Infrastruture.Core;
using KAT_Shop.Web.Infrastruture.Extensions;
using KAT_Shop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/feedback")]
    [Authorize]
    public class FeedbackController : ApiControllerBase
    {
        private IFeedbackService _feedbackService;
        public FeedbackController(IErrorService error, IFeedbackService feedbackService) : base(error)
        {
            _feedbackService = feedbackService;
        }

        [Route("getall")]
        [HttpGet]
        [Authorize(Roles = "Access")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, string keyword, int page, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _feedbackService.GetAll(keyword);

                totalRow = model.Count();
                var query = (pageSize == -1) ? model.OrderByDescending(x => x.CreatedDate) : model.OrderByDescending(x => x.CreatedDate).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<Feedback>, IEnumerable<FeedbackViewModel>>(query);

                var paginationSet = new PaginationSet<FeedbackViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (pageSize == -1) ? 1 : (int)Math.Ceiling((decimal)totalRow / pageSize)
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]
        [Authorize(Roles = "Access")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)

        {
            return CreateHttpResponse(request, () =>
            {
                var model = _feedbackService.GetById(id);

                var responseData = Mapper.Map<Feedback, FeedbackViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getallparents")]
        [HttpGet]
        [Authorize(Roles = "Access")]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _feedbackService.GetAll();

                var responseData = Mapper.Map<IEnumerable<Feedback>, IEnumerable<FeedbackViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getnumfeedbacknew")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetNumberFeedbackNew(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _feedbackService.GetNumberFeedback();
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;      
            });
        }

        [Route("getlatestfeedback")]
        [HttpGet]              
        public HttpResponseMessage GetLatestFeedback(HttpRequestMessage request, int top)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _feedbackService.GetLatestFeedback(top);
                var responseData = Mapper.Map<IEnumerable<Feedback>, IEnumerable<FeedbackViewModel>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("sendfeedback")]
        [HttpPost]                                                                      
        public HttpResponseMessage Create(HttpRequestMessage request, string feedbackViewModel )
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var feedbackVm = new JavaScriptSerializer().Deserialize<FeedbackViewModel>(feedbackViewModel);
                Validate<FeedbackViewModel>(feedbackVm);
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {                           
                        var newFeedback = new Feedback();
                        newFeedback.UpdateFeedback(feedbackVm);
                        newFeedback.CreatedDate = DateTime.Now;
                        _feedbackService.Add(newFeedback);
                        _feedbackService.Save();
                        var responseData = Mapper.Map<Feedback, FeedbackViewModel>(newFeedback);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData); 
                    }
                    catch (Exception ex)
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    }
                }   
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Authorize(Roles = "Edit")]
        public HttpResponseMessage Update(HttpRequestMessage request, FeedbackViewModel feedbackVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbFeedback = _feedbackService.GetById(feedbackVm.ID);
                    try
                    {
                        dbFeedback.UpdateFeedback(feedbackVm);
                        dbFeedback.RepliedBy = User.Identity.Name;
                        dbFeedback.RepliedDate = DateTime.Now;
                        _feedbackService.Update(dbFeedback);

                        //Sendmail
                        string content = System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Assets/admin/templates/feedback_reply.html"));
                        content = content.Replace("{{Name}}", dbFeedback.Name);
                        content = content.Replace("{{Email}}", dbFeedback.Email);
                        content = content.Replace("{{Message}}", dbFeedback.Message);
                        content = content.Replace("{{RepliedContent}}", feedbackVm.RepliedContent);

                        MailHelper.SendMail(dbFeedback.Email, "Thông tin phản hồi từ KATShop", content);
                    }
                    catch
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                    _feedbackService.Save();

                    var responseData = Mapper.Map<Feedback, FeedbackViewModel>(dbFeedback);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldFeedback = _feedbackService.Delete(id);

                    _feedbackService.Save();

                    var responseData = Mapper.Map<Feedback, FeedbackViewModel>(oldFeedback);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedFeedbacks)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listFeedback = new JavaScriptSerializer().Deserialize<List<int>>(checkedFeedbacks);
                    foreach (var item in listFeedback)
                    {
                        _feedbackService.Delete(item);
                    }

                    _feedbackService.Save();


                    response = request.CreateResponse(HttpStatusCode.Created, listFeedback.Count);
                }

                return response;
            });
        }
    }
}
