﻿using KAT_Shop.Data;
using KAT_Shop.Model.Models;
using KAT_Shop.Service;
using KAT_Shop.Web.App_Start;
using KAT_Shop.Web.Infrastruture.Core;
using KAT_Shop.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/home")]
    [Authorize]
    public class HomeController : ApiControllerBase
    {
        IErrorService _errorService;
        private ICommonService _commonService;
        private IProductBrandService _productBrandService;
        private IProductCategoryService _productCategoryService;
        private ApplicationUserManager _userManager;
        private IGroupService _appGroupService;
        private IRoleService _appRoleService;
        public HomeController(IErrorService errorService, ICommonService commonService, IProductBrandService productBrandService, IProductCategoryService productCategoryService, ApplicationUserManager userManager, IGroupService appGroupService, IRoleService appRoleService) : base(errorService)
        {
            _errorService = errorService;
            _commonService = commonService;
            _productBrandService = productBrandService;
            _productCategoryService = productCategoryService;
            _userManager = userManager;
            _appGroupService = appGroupService;
            _appRoleService = appRoleService;
        }

        [HttpGet]
        [Route("TestMethod")]
        [Authorize(Roles = "Access")]
        public string TestMethod()
        {
            return "";
        }

        [HttpGet]
        [Route("seedInit")]
        [AllowAnonymous]
        public HttpResponseMessage SeedInit(HttpRequestMessage request, KAT_ShopDbContext context)
        {
            try
            {
                CreateRole();
                CreateGroup();
                CreateUser();
                CreateProductBrandSample();
                CreateProductCategorySample();
                CreateSystemConfigSample();
                CreateShopInfoSample();
                return request.CreateResponse(HttpStatusCode.OK, "Tạo dữ liệu mẫu thành công!");
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }

        private void CreateRole()
        {
            var roleManager = new RoleManager<Role>(new RoleStore<Role>(new KAT_ShopDbContext()));
            if (!roleManager.Roles.Any(x => x.Name == DefaultRoleApplication.Access.ToString()))
            {
                roleManager.Create(new Role { Name = DefaultRoleApplication.Access.ToString(), Description = "Truy cập" });
            }
            if (!roleManager.Roles.Any(x => x.Name == DefaultRoleApplication.Edit.ToString()))
            {
                roleManager.Create(new Role { Name = DefaultRoleApplication.Edit.ToString(), Description = "Chỉnh sửa" });
            }
            if (!roleManager.Roles.Any(x => x.Name == DefaultRoleApplication.Add.ToString()))
            {
                roleManager.Create(new Role { Name = DefaultRoleApplication.Add.ToString(), Description = "Thêm mới" });
            }
            if (!roleManager.Roles.Any(x => x.Name == DefaultRoleApplication.Delete.ToString()))
            {
                roleManager.Create(new Role { Name = DefaultRoleApplication.Delete.ToString(), Description = "Xóa" });
            }
        }

        private void CreateGroup()
        {
            var roleManager = new RoleManager<Role>(new RoleStore<Role>(new KAT_ShopDbContext()));
            if (!_appGroupService.GetAll().Any(x => x.Name == DefaultGroupApplication.Admin.ToString()))
            {
                try
                {
                    _appGroupService.Add(new Group() { Name = DefaultGroupApplication.Admin.ToString(), Description = "Quản trị" });
                    _appGroupService.Save();
                    var groupId = _appGroupService.GetByGroupName(DefaultGroupApplication.Admin.ToString()).ID;
                    List<RoleGroup> listRoleGroup = new List<RoleGroup>()
                  {
                      new RoleGroup() { RoleId = roleManager.FindByName(DefaultRoleApplication.Access.ToString()).Id.ToString(), GroupId=groupId},
                      new RoleGroup() { RoleId = roleManager.FindByName(DefaultRoleApplication.Edit.ToString()).Id.ToString(), GroupId=groupId },
                      new RoleGroup() { RoleId = roleManager.FindByName(DefaultRoleApplication.Add.ToString()).Id.ToString(), GroupId=groupId},
                      new RoleGroup() { RoleId = roleManager.FindByName(DefaultRoleApplication.Delete.ToString()).Id.ToString(), GroupId=groupId}
                  };
                    _appRoleService.AddRolesToGroup(listRoleGroup, groupId);
                    _appGroupService.Save();
                }
                catch (Exception ex)
                {
                    _appGroupService.Delete(_appGroupService.GetByGroupName(DefaultGroupApplication.Admin.ToString()).ID);
                    _appGroupService.Save();
                }

            }
            if (!_appGroupService.GetAll().Any(x => x.Name == DefaultGroupApplication.Customer.ToString()))
            {
                _appGroupService.Add(new Group() { Name = DefaultGroupApplication.Customer.ToString(), Description = "Khách hàng" });
            }
        }

        private void CreateUser()
        {
            if (!_userManager.Users.Any(x=>x.UserName == DefaultAccountApplication.admin.ToString()))
            {
                var manager = new UserManager<User>(new UserStore<User>(new KAT_ShopDbContext()));

                var roleManager = new RoleManager<Role>(new RoleStore<Role>(new KAT_ShopDbContext()));

                var user = new User()
                {
                    UserName = DefaultAccountApplication.admin.ToString(),
                    Email = "ttkhai1995vl@gmail.com",
                    EmailConfirmed = true,
                    BirthDay = DateTime.Now,
                    FullName = "Administrator"
                };

                manager.Create(user, "123654$");

                var adminUser = manager.FindByEmail("ttkhai1995vl@gmail.com");

                manager.AddToRoles(adminUser.Id, new string[] { "Access", "Add", "Edit", "Delete" });
            }
            //Create user for app android
            if (!_userManager.Users.Any(x => x.UserName == DefaultAccountApplication.appkat.ToString()))
            {
                var manager = new UserManager<User>(new UserStore<User>(new KAT_ShopDbContext()));
                var userApp = new User()
                {
                    UserName = DefaultAccountApplication.appkat.ToString(),
                    Email = "thisinhctu97@gmail.com",
                    EmailConfirmed = true,
                    BirthDay = DateTime.Now,
                    FullName = "Ứng dụng Android"
                };
                manager.Create(userApp, "app@kat2016");
            }
        }

        private void CreateProductCategorySample()
        {
            if (_productCategoryService.GetAll().Count() == 0)
            {
                List<ProductCategory> listProductCategory = new List<ProductCategory>()
            {
                new ProductCategory() { Name="Máy tính bảng", Alias="may-tinh-bang", IsDelete=false, IsPublished=true },
                new ProductCategory() { Name="Điện thoại", Alias="dien-thoai", IsDelete=false, IsPublished=true } ,
                new ProductCategory() { Name="Máy tính", Alias="may-tinh", IsDelete=false, IsPublished=true }
            };
                foreach(var item in listProductCategory)
                {
                    _productCategoryService.Add(item);
                }
                _productCategoryService.Save();
            }
        }

        private void CreateProductBrandSample()
        {
            if (_productBrandService.GetAll().Count() == 0)
            {
                List<ProductBrand> listProductBrand = new List<ProductBrand>()
            {
                new ProductBrand() { Name="Apple", Alias="apple", IsDelete=false, IsPublished=true },
                new ProductBrand() { Name="Samsung", Alias="samsung", IsDelete=false, IsPublished=true },
                new ProductBrand() { Name="Asus", Alias="asus", IsDelete=false, IsPublished=true },
                new ProductBrand() { Name="HTC", Alias="htc", IsDelete=false, IsPublished=true }
            };
                foreach (var productBrand in listProductBrand)
                {
                    _productBrandService.Add(productBrand);
                }
                _productBrandService.Save();
            }
        }

        private void CreateSystemConfigSample()
        {
            List<SystemConfig> listSystemConfig = new List<SystemConfig>();
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Pending"))
            {
                _commonService.Create("Pending", "Chưa xử lý", 10);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Processing"))
            {
                _commonService.Create("Processing", "Đang xử lý", 20);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Unconfirmed"))
            {
                _commonService.Create("Unconfirmed", "Chưa xác nhận", 30);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Confirmed"))
            {
                _commonService.Create("Confirmed", "Đã xác nhận", 40);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Complete"))
            {
                _commonService.Create("Complete", "Đã hoàn thành", 50);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Cancelled"))
            {
                _commonService.Create("Cancelled", "Hủy", 60);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "HasShipped"))
            {
                _commonService.Create("HasShipped", "Đã gửi đi", 70);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Packed"))
            {
                _commonService.Create("Packed", "Đã đóng gói", 80);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Unpaid"))
            {
                _commonService.Create("Unpaid", "Chưa thanh toán", 11);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "Paid"))
            {
                _commonService.Create("Paid", "Đã thanh toán", 21);
            }
            _commonService.SaveChange();
        }

        private void CreateShopInfoSample()
        {
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "ShopName"))
            {
                _commonService.Create("ShopName", "KatShop", null);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "ShopPhone"))
            {
                _commonService.Create("ShopPhone", "09999999999", null);
            }
            if (!_commonService.GetAllSystemConfig().Any(x => x.Code == "ShopAddress"))
            {
                _commonService.Create("ShopAddress", "Cần Thơ", null);
            }
            _commonService.SaveChange();
        }
    }
}
