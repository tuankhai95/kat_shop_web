﻿using AutoMapper;
using KAT_Shop.Model.Models;
using KAT_Shop.Service;
using KAT_Shop.Web.Infrastruture.Core;
using KAT_Shop.Web.Infrastruture.Extensions;
using KAT_Shop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/product")]
    [Authorize]
    public class ProductController : ApiControllerBase
    {
        private IProductService _productService;
        private IProductAttributeValueService _productAttributeValueService;
        private IProductAttributeService _productAttributeService;

        public ProductController(IErrorService errorService, IProductService productService, IProductAttributeValueService productAttributeValueService, IProductAttributeService productAttributeService) : base(errorService)
        {
            _productService = productService;
            _productAttributeValueService = productAttributeValueService;
            _productAttributeService = productAttributeService;
        }

        [Route("getall")]
        [HttpGet]
        [Authorize(Roles = "Access")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, string keyword, int page, int pageSize = 20, 
            int categoryID = 0, int brandID = 0, int havePublished =0, int showHomePage=0, int sortQuantity=0)
        {
            return CreateHttpResponse(request, () =>
            {   
                int totalRow = 0;
                var model = _productService.GetAll(keyword);
                if (categoryID != 0 && brandID == 0)
                {
                    model = model.Where(x => x.CategoryID == categoryID);
                }
                else if (categoryID == 0 && brandID != 0)
                {
                    model = model.Where(x => x.BrandID == brandID);
                }
                else if(categoryID != 0 && brandID != 0)
                {
                    model = model.Where(x => x.CategoryID == categoryID && x.BrandID == brandID);
                }
                //Check Ispublished
                if (havePublished == 1)
                {
                    model = model.Where(x => x.IsPublished == true);
                } else if(havePublished == 2)
                {
                    model = model.Where(x => x.IsPublished == false);
                }
                //Check showHomepage
                if (showHomePage == 1)
                {
                    model = model.Where(x => x.HomeFlag == true);
                }
                else if (showHomePage == 2)
                {
                    model = model.Where(x => x.HomeFlag == null);
                }

                totalRow = model.Count();
                var query = model;
                if (sortQuantity == 1)
                {
                    query = (pageSize == -1) ? model.OrderBy(x => x.Quantity) : model.OrderBy(x => x.Quantity).Skip(page * pageSize).Take(pageSize);
                }
                else  if(sortQuantity==2)
                {
                    query = (pageSize == -1) ? model.OrderByDescending(x => x.Quantity) : model.OrderByDescending(x => x.Quantity).Skip(page * pageSize).Take(pageSize);
                }
                else
                {
                    query = (pageSize == -1) ? model.OrderBy(x => x.Name) : model.OrderBy(x => x.Name).Skip(page * pageSize).Take(pageSize);
                }     
                var responseData = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(query);

                var paginationSet = new PaginationSet<ProductViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (pageSize == -1) ? 1 : (int)Math.Ceiling((decimal)totalRow / pageSize)
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)

        {
            return CreateHttpResponse(request, () =>
            {
                var product = _productService.GetById(id);

                var responseData = Mapper.Map<Product, ProductDetailViewModel>(product);

                if (product == null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không tìm thấy!");
                }
                _productService.IncreaseView(id);
                var listAttribute = _productAttributeService.GetListAttributeByProductID(product.ID);

                responseData.AttributeValues = Mapper.Map<IEnumerable<ProductAttributeValue>, IEnumerable<ProductAttributeValueViewModel>>(listAttribute);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getproductbyid/{id:int}")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetProductById(HttpRequestMessage request, int id)

        {
            return CreateHttpResponse(request, () =>
            {
                var product = _productService.GetById(id);

                var responseData = Mapper.Map<Product, ProductViewModel>(product);

                if (product == null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không tìm thấy!");
                }
                var listAttribute = _productAttributeService.GetListAttributeByProductID(product.ID);

                responseData.AttributeValues = Mapper.Map<IEnumerable<ProductAttributeValue>, IEnumerable<ProductAttributeValueViewModel>>(listAttribute);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getallparents")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productService.GetAll().OrderBy(x => x.Name);

                var responseData = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductDetailViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getlatestproduct")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetLatestProduct(HttpRequestMessage request, int top)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productService.GetLastest(top);
                var responseData = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("gethotproduct")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetHotProduct(HttpRequestMessage request, int top)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productService.GetHotProduct(top);
                var responseData = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getlistproductbycategoryid")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetListProductByCategoryId(HttpRequestMessage request, int categoryId)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productService.GetListProductByCategoryId(categoryId);
                var responseData = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getlistproductbybrandid")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetListProductByBrandId(HttpRequestMessage request, int brandId)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productService.GetListProductByBrandId(brandId);
                var responseData = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getlistproductbyname")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetListProductByName(HttpRequestMessage request, string productName)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productService.GetListProductByName(productName);
                var responseData = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("create")]
        [HttpPost]
        [Authorize(Roles = "Add")]
        public HttpResponseMessage Create(HttpRequestMessage request, ProductViewModel productVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        var newProduct = new Product();
                        newProduct.UpdateProduct(productVm);
                        newProduct.CreatedDate = DateTime.Now;
                        newProduct.CreatedBy = User.Identity.Name;
                        _productService.Add(newProduct);
                        _productService.Save();
                        try
                        {
                            var attributeValue = new List<ProductAttributeValue>();
                            foreach (var attribute in productVm.AttributeValues)
                            {
                                attributeValue.Add(new ProductAttributeValue()
                                {
                                    AttributeId = attribute.ID,
                                    ProductId = newProduct.ID,
                                    Value = attribute.Value
                                });
                            }
                            _productAttributeValueService.AddAttributeValue(attributeValue, newProduct.ID);
                            _productAttributeValueService.Save();
                        }
                        catch
                        {
                            _productService.Remove(newProduct.ID);
                            _productService.Save();
                        }

                        var responseData = Mapper.Map<Product, ProductViewModel>(newProduct);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    catch (Exception ex)
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                    }

                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Authorize(Roles = "Edit")]
        public HttpResponseMessage Update(HttpRequestMessage request, ProductViewModel productVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        var dbProduct = _productService.GetById(productVm.ID);
                        dbProduct.UpdateProduct(productVm);
                        dbProduct.UpdatedDate = DateTime.Now;
                        dbProduct.UpdatedBy = User.Identity.Name;
                        _productService.Update(dbProduct);

                        //Add new items into table ProductAttributeValue                                    
                        var newproductAttributeValue = new List<ProductAttributeValue>();
                        var oldProductAttributeValues = _productAttributeValueService.GetListByID(dbProduct.ID);
                        var listID = new List<int>();
                        foreach (var productAttributeVm in productVm.AttributeValues)
                        {
                            if (oldProductAttributeValues.Any(x => x.ID == productAttributeVm.ID))
                            {
                                var productAttributeEdit = _productAttributeValueService.GetById(productAttributeVm.ID);
                                productAttributeEdit.UpdateProductAttributeValue(productAttributeVm);
                                _productAttributeValueService.Update(productAttributeEdit);
                                listID.Add(productAttributeVm.ID);
                                continue;
                            }
                            newproductAttributeValue.Add(new ProductAttributeValue()
                            {
                                ProductId = dbProduct.ID,
                                AttributeId = productAttributeVm.AttributeId,
                                Value = productAttributeVm.Value
                            });
                        }
                        _productAttributeValueService.AddAttributeValue(newproductAttributeValue, dbProduct.ID);

                        _productAttributeValueService.Save();
                        foreach (var newProducAttr in newproductAttributeValue)
                        {
                            listID.Add(_productAttributeValueService.GetListIDByAttrIDProID(newProducAttr));
                        }

                        //Delete items in ProductTemplateAttribute
                        var deletedAttributes = oldProductAttributeValues.Where(attr => !listID.Contains(attr.ID));
                        foreach (var deletedAttributeValue in deletedAttributes)
                        {
                            _productAttributeValueService.Delete(deletedAttributeValue.ID);
                        }
                        _productAttributeValueService.Save();

                        var responseData = Mapper.Map<Product, ProductViewModel>(dbProduct);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    catch (Exception ex)
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                    }

                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProduct = _productService.Delete(id);
                    _productService.Save();

                    var responseData = Mapper.Map<Product, ProductViewModel>(oldProduct);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProducts)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listProduct = new JavaScriptSerializer().Deserialize<List<int>>(checkedProducts);
                    var oldproduct = new Product();
                    foreach (var item in listProduct)
                    {
                        oldproduct = _productService.Delete(item);
                    }

                    _productService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, listProduct.Count);
                }

                return response;
            });
        }

    }
}
