﻿using AutoMapper;
using KAT_Shop.Common.Exceptions;
using KAT_Shop.Model.Models;
using KAT_Shop.Service;
using KAT_Shop.Web.Infrastruture.Core;
using KAT_Shop.Web.Infrastruture.Extensions;
using KAT_Shop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/producttemplate")]
    [Authorize(Roles ="Access")]
    public class ProductTemplateController : ApiControllerBase
    {
        private IProductTemplateService _productTemplateService;
        private IProductTemplateAttributeService _productTemplateAttributeService;
        private IProductAttributeService _productAttributeService;
        public ProductTemplateController(IErrorService errorService, IProductTemplateService productTemplateService, IProductTemplateAttributeService productTemplateAttributeService, IProductAttributeService productAttributeService) : base(errorService)
        {
            _productTemplateService = productTemplateService;
            _productTemplateAttributeService = productTemplateAttributeService;
            _productAttributeService = productAttributeService;
        }

        [Route("getall")]
        [HttpGet]                                     
        public HttpResponseMessage GetAll(HttpRequestMessage request, string keyword, int page, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _productTemplateService.GetAll(keyword);

                totalRow = model.Count();
                var query = (pageSize == -1) ? model.OrderBy(x=>x.Name) : model.OrderBy(x => x.Name).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<ProductTemplate>, IEnumerable<ProductTemplateViewModel>>(query);

                var paginationSet = new PaginationSet<ProductTemplateViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (pageSize == -1) ? 1 : (int)Math.Ceiling((decimal)totalRow / pageSize)
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]                                     
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var productTemplate = _productTemplateService.GetById(id);

                var responseData = Mapper.Map<ProductTemplate, ProductTemplateViewModel>(productTemplate);

                if (productTemplate == null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không tìm thấy!");
                }
                var listAttribute = _productAttributeService.GetListAttributeByTemplateID(productTemplate.ID);
                responseData.ProductAttributes = Mapper.Map<IEnumerable<ProductAttribute>, IEnumerable<ProductAttributeViewModel>>(listAttribute);
                return request.CreateResponse(HttpStatusCode.OK, responseData);
            });
        }

        [Route("getallparents")]
        [HttpGet]                                     
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productTemplateService.GetAll().OrderBy(x=>x.Name);

                var responseData = Mapper.Map<IEnumerable<ProductTemplate>, IEnumerable<ProductTemplateViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("create")]
        [HttpPost]
        [Authorize(Roles ="Add")]
        public HttpResponseMessage Create(HttpRequestMessage request, ProductTemplateViewModel productTemplateVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        var newProductTemplate = new ProductTemplate();
                        newProductTemplate.UpdateProductTemplate(productTemplateVm);
                        var template = _productTemplateService.Add(newProductTemplate);
                        _productTemplateService.Save();

                        try
                        {
                            var newproductAttribute = new List<ProductTemplateAttribute>();       
                            foreach (var productAttributeVm in productTemplateVm.ProductAttributes)
                            {
                                newproductAttribute.Add(new ProductTemplateAttribute()
                                {
                                    ProductAttributeId = productAttributeVm.ID,
                                    ProductTemplateId = template.ID
                                });
                            }
                            _productTemplateAttributeService.AddAttributeToTemplate(newproductAttribute, newProductTemplate.ID);
                        }
                        catch
                        {
                            _productTemplateService.Delete(template.ID);
                            _productTemplateService.Save();
                        }
                        _productTemplateAttributeService.Save();
                        var responseData = Mapper.Map<ProductTemplate, ProductTemplateViewModel>(newProductTemplate);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    catch (NameDuplicatedException ex)
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                    }
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Authorize(Roles ="Edit")]
        public HttpResponseMessage Update(HttpRequestMessage request, ProductTemplateViewModel productTemplateVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        var dbProductTemplate = _productTemplateService.GetById(productTemplateVm.ID);
                        dbProductTemplate.UpdateProductTemplate(productTemplateVm);
                        _productTemplateService.Update(dbProductTemplate);
                        _productTemplateService.Save();

                        //Add new items into table ProductTemplateAttribute
                        var newproductAttribute = new List<ProductTemplateAttribute>();
                        var oldProductTemplateAttribute = _productTemplateAttributeService.GetListByTemplateID(dbProductTemplate.ID);
                        foreach (var productAttributeVm in productTemplateVm.ProductAttributes)
                        {
                            if (oldProductTemplateAttribute.Any(x => x.ProductAttributeId == productAttributeVm.ID))
                            {
                                continue;
                            }
                            newproductAttribute.Add(new ProductTemplateAttribute()
                            {
                                ProductAttributeId = productAttributeVm.ID,
                                ProductTemplateId = dbProductTemplate.ID
                            });
                        }
                        _productTemplateAttributeService.AddAttributeToTemplate(newproductAttribute, dbProductTemplate.ID);
                        _productTemplateAttributeService.Save();
                        var responseData = Mapper.Map<ProductTemplate, ProductTemplateViewModel>(dbProductTemplate);
                        //Delete items in ProductTemplateAttribute
                        var deletedAttributes = oldProductTemplateAttribute.Where(attr => !productTemplateVm.ProductAttributes.Select(x => x.ID).Contains(attr.ProductAttributeId));
                        
                        foreach (var deletedAttribute in deletedAttributes)
                        {
                            var old = _productTemplateAttributeService.Delete(deletedAttribute);                       
                        }
                        _productTemplateAttributeService.Save();
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    catch (NameDuplicatedException ex)
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                    }
                }
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize(Roles ="Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductTemplate = _productTemplateService.Delete(id);

                    _productTemplateService.Save();

                    var responseData = Mapper.Map<ProductTemplate, ProductTemplateViewModel>(oldProductTemplate);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Authorize(Roles ="Delete")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductTemplates)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listProductTemplate = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductTemplates);
                    foreach (var item in listProductTemplate)
                    {
                        _productTemplateService.Delete(item);
                    }

                    _productTemplateService.Save();


                    response = request.CreateResponse(HttpStatusCode.Created, listProductTemplate.Count);
                }

                return response;
            });
        }
    }
}
