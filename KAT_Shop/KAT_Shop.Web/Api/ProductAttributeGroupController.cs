﻿using AutoMapper;
using KAT_Shop.Model.Models;
using KAT_Shop.Service;
using KAT_Shop.Web.Infrastruture.Core;
using KAT_Shop.Web.Infrastruture.Extensions;
using KAT_Shop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/productattributegroup")]
    [Authorize(Roles = "Access")]
    public class ProductAttributeGroupController : ApiControllerBase
    {
        private IProductAttributeGroupService _productAttributeGroupService;
        public ProductAttributeGroupController(IErrorService errorService, IProductAttributeGroupService productAttributeGroupService) : base(errorService)
        {
            _productAttributeGroupService = productAttributeGroupService;
        }
        [Route("getall")]
        [HttpGet]            
        public HttpResponseMessage GetAll(HttpRequestMessage request, string keyword, int page, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _productAttributeGroupService.GetAll(keyword);

                totalRow = model.Count();
                var query = (pageSize == -1) ? model.OrderBy(x => x.Name) : model.OrderBy(x => x.Name).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<ProductAttributeGroup>, IEnumerable<ProductAttributeGroupViewModel>>(query);

                var paginationSet = new PaginationSet<ProductAttributeGroupViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (pageSize == -1) ? 1 : (int)Math.Ceiling((decimal)totalRow / pageSize)
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]             
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)

        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productAttributeGroupService.GetById(id);

                var responseData = Mapper.Map<ProductAttributeGroup, ProductAttributeGroupViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getallparents")]
        [HttpGet]           
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productAttributeGroupService.GetAll().OrderBy(x => x.Name);

                var responseData = Mapper.Map<IEnumerable<ProductAttributeGroup>, IEnumerable<ProductAttributeGroupViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("create")]
        [HttpPost]
        [Authorize(Roles = "Add")]
        public HttpResponseMessage Create(HttpRequestMessage request, ProductAttributeGroupViewModel productAttributeGroupVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newProductAttributeGroup = new ProductAttributeGroup();
                    newProductAttributeGroup.UpdateProductAttributeGroup(productAttributeGroupVm);
                    _productAttributeGroupService.Add(newProductAttributeGroup);
                    _productAttributeGroupService.Save();

                    var responseData = Mapper.Map<ProductAttributeGroup, ProductAttributeGroupViewModel>(newProductAttributeGroup);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Authorize(Roles = "Edit")]
        public HttpResponseMessage Update(HttpRequestMessage request, ProductAttributeGroupViewModel productAttributeGroupVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbProductAttributeGroup = _productAttributeGroupService.GetById(productAttributeGroupVm.ID);
                    dbProductAttributeGroup.UpdateProductAttributeGroup(productAttributeGroupVm);
                    _productAttributeGroupService.Update(dbProductAttributeGroup);
                    _productAttributeGroupService.Save();

                    var responseData = Mapper.Map<ProductAttributeGroup, ProductAttributeGroupViewModel>(dbProductAttributeGroup);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductAttributeGroup = _productAttributeGroupService.Delete(id);

                    _productAttributeGroupService.Save();

                    var responseData = Mapper.Map<ProductAttributeGroup, ProductAttributeGroupViewModel>(oldProductAttributeGroup);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductAttributeGroups)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listProductAttributeGroup = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductAttributeGroups);
                    foreach (var item in listProductAttributeGroup)
                    {
                        _productAttributeGroupService.Delete(item);
                    }

                    _productAttributeGroupService.Save();


                    response = request.CreateResponse(HttpStatusCode.Created, listProductAttributeGroup.Count);
                }

                return response;
            });
        }

    }
}
