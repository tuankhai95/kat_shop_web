﻿using AutoMapper;
using KAT_Shop.Model.Models;
using KAT_Shop.Service;
using KAT_Shop.Web.Infrastruture.Core;
using KAT_Shop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/systemconfig")]
    [Authorize]
    public class SystemConfigController : ApiControllerBase
    {
        private ICommonService _commonService;
        public SystemConfigController(IErrorService errorService, ICommonService commonService) : base(errorService)
        {
            _commonService = commonService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("getshopinfo")]
        public HttpResponseMessage GetShopInfo(HttpRequestMessage request)
        {
            var shopInfo = new ShopInfoViewModel();
            shopInfo.ShopName = _commonService.GetSystemConfig("ShopName").ValueString;
            shopInfo.ShopAddress = _commonService.GetSystemConfig("ShopAddress").ValueString;
            shopInfo.ShopPhone = _commonService.GetSystemConfig("ShopPhone").ValueString;

            var response = request.CreateResponse(HttpStatusCode.OK, shopInfo);
            return response;
        }

        [Route("getorderstatus")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetOrderStatusConfig(HttpRequestMessage request, string listCode)
        {
            var list = new JavaScriptSerializer().Deserialize<List<string>>(listCode);
            var model = _commonService.GetAllSystemConfig().Where(x => list.Contains(x.Code));
            var responseData = Mapper.Map<IEnumerable<SystemConfig>, IEnumerable<SystemConfigViewModel>>(model);
            var response = request.CreateResponse(HttpStatusCode.OK, responseData);
            return response;
        }
    }
}
