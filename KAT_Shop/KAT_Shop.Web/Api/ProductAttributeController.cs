﻿using AutoMapper;
using KAT_Shop.Model.Models;
using KAT_Shop.Service;
using KAT_Shop.Web.Infrastruture.Core;
using KAT_Shop.Web.Infrastruture.Extensions;
using KAT_Shop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace KAT_Shop.Web.Api
{
    [RoutePrefix("api/productattribute")]
    [Authorize(Roles = "Access")]
    public class ProductAttributeController : ApiControllerBase
    {
        private IProductAttributeService _productAttributeService;
        public ProductAttributeController(IErrorService errorService, IProductAttributeService productAttributeService) : base(errorService)
        {
            _productAttributeService = productAttributeService;
        }

        [Route("getall")]
        [HttpGet]          
        public HttpResponseMessage GetAll(HttpRequestMessage request, string keyword, int page, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _productAttributeService.GetAll(keyword);

                totalRow = model.Count();
                var query = (pageSize == -1) ? model.OrderBy(x => x.Name) : model.OrderBy(x => x.Name).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<ProductAttribute>, IEnumerable<ProductAttributeViewModel>>(query);

                var paginationSet = new PaginationSet<ProductAttributeViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,
                    TotalPages = (pageSize == -1) ? 1 : (int)Math.Ceiling((decimal)totalRow / pageSize)
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getallparents")]
        [HttpGet]          
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productAttributeService.GetAll().OrderBy(x => x.Name);

                var responseData = Mapper.Map<IEnumerable<ProductAttribute>, IEnumerable<ProductAttributeViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getbyid/{id:int}")]
        [HttpGet]           
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)

        {
            return CreateHttpResponse(request, () =>
            {
                var model = _productAttributeService.GetById(id);

                var responseData = Mapper.Map<ProductAttribute, ProductAttributeViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("create")]
        [HttpPost]
        [Authorize(Roles = "Add")]
        public HttpResponseMessage Create(HttpRequestMessage request, ProductAttributeViewModel productAttributeVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newProductAttribute = new ProductAttribute();
                    newProductAttribute.UpdateProductAttribute(productAttributeVm);
                    _productAttributeService.Add(newProductAttribute);
                    _productAttributeService.Save();

                    var responseData = Mapper.Map<ProductAttribute, ProductAttributeViewModel>(newProductAttribute);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Authorize(Roles = "Edit")]
        public HttpResponseMessage Update(HttpRequestMessage request, ProductAttributeViewModel productAttributeVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbProductAttribute = _productAttributeService.GetById(productAttributeVm.ID);
                    dbProductAttribute.UpdateProductAttribute(productAttributeVm);
                    _productAttributeService.Update(dbProductAttribute);
                    _productAttributeService.Save();

                    var responseData = Mapper.Map<ProductAttribute, ProductAttributeViewModel>(dbProductAttribute);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductAttribute = _productAttributeService.Delete(id);

                    _productAttributeService.Save();

                    var responseData = Mapper.Map<ProductAttribute, ProductAttributeViewModel>(oldProductAttribute);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Authorize(Roles = "Delete")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductAttributes)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listProductAttribute = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductAttributes);
                    foreach (var item in listProductAttribute)
                    {
                        _productAttributeService.Delete(item);
                    }

                    _productAttributeService.Save();


                    response = request.CreateResponse(HttpStatusCode.Created, listProductAttribute.Count);
                }

                return response;
            });
        }

    }
}
