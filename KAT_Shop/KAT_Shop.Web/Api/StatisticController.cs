﻿using KAT_Shop.Common.ViewModel;
using KAT_Shop.Service;
using KAT_Shop.Web.Infrastruture.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KAT_Shop.Web.Api
{
    [Authorize(Roles ="Access")]
    [RoutePrefix("api/statistic")]
    public class StatisticController : ApiControllerBase
    {
        IStatisticService _statisticService;
        public StatisticController(IErrorService errorService, IStatisticService statisticService) : base(errorService)
        {
            _statisticService = statisticService;
        }

        [Route("getrevenue")]
        [HttpGet]
        public HttpResponseMessage GetRevenueStatistic(HttpRequestMessage request, string fromDate, string toDate)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _statisticService.GetRevenueStatistic(fromDate, toDate);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("getproductstatistic")]
        [HttpGet]
        public HttpResponseMessage GetProductStatistic(HttpRequestMessage request, string fromDate, string toDate, int sort, int top)
        {
            return CreateHttpResponse(request, () =>
            {
                //top = -1 is getall
                var model = new List<ProductStatisticViewModel>();
                if (sort == (int)DefaultListSort.Poorly_Selling_Product)           //Poorly selling product  
                {
                    model.AddRange((top == -1) ? _statisticService.GetProductStatistic(fromDate, toDate).OrderBy(x => x.Quantities) : _statisticService.GetProductStatistic(fromDate, toDate).OrderBy(x => x.Quantities).Take(top));
                }
                else if (sort == (int)DefaultListSort.More_Selling_Product)  //Selling products
                {
                    model.AddRange((top == -1) ? _statisticService.GetProductStatistic(fromDate, toDate).OrderByDescending(x => x.Quantities) : _statisticService.GetProductStatistic(fromDate, toDate).OrderByDescending(x => x.Quantities).Take(top));
                }
                else if (sort == (int)DefaultListSort.Benefit_Low)     //Benefit low
                {
                    model.AddRange((top == -1) ? _statisticService.GetProductStatistic(fromDate, toDate).OrderBy(x => x.Benefit) : _statisticService.GetProductStatistic(fromDate, toDate).OrderBy(x => x.Benefit).Take(top));
                }
                else if (sort == (int)DefaultListSort.Benefit_High)    //Benefit high
                {
                    model.AddRange((top == -1) ? _statisticService.GetProductStatistic(fromDate, toDate).OrderByDescending(x => x.Benefit) : _statisticService.GetProductStatistic(fromDate, toDate).OrderByDescending(x => x.Benefit).Take(top));
                }
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }


    }
}
