﻿using System;

namespace KAT_Shop.Web.Models
{
    public class ErrorViewModel
    {
        public int ID { get; set; }

        public string Message { get; set; }

        public string StrackTrace { get; set; }
                     
        public DateTime CreatedDate { get; set; }
    }
}