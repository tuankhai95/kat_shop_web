﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{
    [Serializable]
    public class ProductViewModel : ProductDetailViewModel
    {
        public decimal OriginalPrice { get; set; }
    }

    [Serializable]
    public class ProductDetailViewModel
    {
        public int ID { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Name { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Alias { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        public int CategoryID { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        public int BrandID { get; set; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Image { set; get; }

        public string MoreImages { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        public decimal Price { set; get; }

        public decimal? PromotionPrice { set; get; }

        public int? Warranty { set; get; }

        [MaxLength(500, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_500)]
        public string Description { set; get; }

        public string Content { set; get; }

        public string Tags { get; set; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        public int Quantity { get; set; }

        public string NormalizedName { get; set; }

        public bool? HomeFlag { set; get; }

        public bool? HotFlag { set; get; }

        public int? ViewCount { set; get; }

        public bool IsDelete { get; set; }

        public bool IsPublished { get; set; }

        public DateTime? CreatedDate { set; get; }

        [MaxLength(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string CreatedBy { set; get; }

        public DateTime? UpdatedDate { set; get; }

        [MaxLength(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string UpdatedBy { set; get; }

        [MaxLength(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string MetaKeyword { set; get; }

        [MaxLength(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string MetaDescription { set; get; }

        public virtual ProductBrandViewModel ProductBrand { get; set; }

        public virtual ProductCategoryViewModel ProductCategory { set; get; }

        public virtual IEnumerable<ProductTagViewModel> ProductTags { get; set; }

        public virtual IEnumerable<ProductAttributeValueViewModel> AttributeValues { get; set; }

        public virtual IEnumerable<ProductAttributeViewModel> ProductAttributes { get; set; }
    }
}