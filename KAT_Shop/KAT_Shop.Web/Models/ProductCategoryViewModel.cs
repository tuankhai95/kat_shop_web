﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{
    public class ProductCategoryViewModel
    {
        public int ID { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [MaxLength(256,ErrorMessage =Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Name { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Alias { set; get; }

        public string Description { set; get; }

        public int? ParentID { set; get; }

        public int? DisplayOrder { set; get; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Image { set; get; }

        public virtual IEnumerable<ProductViewModel> Product { set; get; }

        public DateTime? CreatedDate { set; get; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string CreatedBy { set; get; }

        public DateTime? UpdatedDate { set; get; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string UpdatedBy { set; get; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string MetaKeyword { set; get; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string MetaDescription { set; get; }

        public bool? HomeFlag { set; get; }

        public bool IsDelete { set; get; }

        public bool IsPublished { get; set; }
    }
}