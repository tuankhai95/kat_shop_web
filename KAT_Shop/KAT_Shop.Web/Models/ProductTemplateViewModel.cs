﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{
    public class ProductTemplateViewModel
    {
        public int ID { set; get; }

        [Required(ErrorMessage =Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [MaxLength(256, ErrorMessage =Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Name { get; set; }

        public virtual IEnumerable<ProductAttributeViewModel> ProductAttributes { get; set; }
        public virtual IEnumerable<ProductTemplateAttributeViewModel> ProductTemplateAttributes { get; set; }

    }
}