﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KAT_Shop.Web.Models
{
    [Serializable]
    public class PostViewModel
    {
        public int ID { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [StringLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Name { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [StringLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Alias { set; get; }
       
        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        public int CategoryID { set; get; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Image { set; get; }
                                              
        public string Description { set; get; }
                                       
        public string Content { set; get; }
                                              
        public bool? HomeFlag { set; get; }
                                              
        public bool? HotFlag { set; get; }
                                              
        public int? ViewCount { set; get; }

        public DateTime? CreatedDate { set; get; }

        [MaxLength(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string CreatedBy { set; get; }

        public DateTime? UpdatedDate { set; get; }

        [MaxLength(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string UpdatedBy { set; get; }

        [MaxLength(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string MetaKeyword { set; get; }

        [MaxLength(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string MetaDescription { set; get; }
                 
        public bool IsPublished { set; get; }

        public virtual PostCategoryViewModel PostCategory { set; get; }
        
        public string NormalizedName { get; set; }
    }
}