﻿using KAT_Shop.Model.Models;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{           
    public class OrderDetailCart
    {
        [Required(ErrorMessage = "Mã sản phẩm là bắt buộc")]
        public int ProductID { set; get; }

        [Range(1, int.MaxValue, ErrorMessage = "Số lượng phải lớn hon 0.")]
        public int Quantity { set; get; }

        public decimal Price { get; set; }
    }       

    public class OrderDetailShowViewModel : OrderDetailCart
    {
        [Required]
        public int OrderID { set; get; }          

        public virtual Order Order { set; get; }

        public virtual Product Product { set; get; }
    }

    public class OrderDetailViewModel : OrderDetailShowViewModel
    {
        [Required]
        public decimal OriginalPrice { get; set; }       
    }
}