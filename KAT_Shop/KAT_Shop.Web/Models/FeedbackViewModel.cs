﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{
    [Serializable]
    public class FeedbackViewModel
    {
        public int ID { get; set; }
 
        [MaxLength(256,ErrorMessage = "Tên không quá 256 ký tự")]
        [Required(ErrorMessage = "Chưa nhập tên.")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Chưa nhập email.")]
        [MaxLength(256, ErrorMessage = "Email không quá 256 ký tự")]
        [EmailAddress(ErrorMessage ="Email chưa đúng.")]
        public string Email { get; set; }
  
        public string Message { get; set; }
                                            
        public DateTime CreatedDate { get; set; }
                                                                
        public bool Status { get; set; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string RepliedBy { get; set; }

        public DateTime? RepliedDate { get; set; }

        public string RepliedContent { get; set; }                 

    }
}