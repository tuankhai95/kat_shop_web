﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{
    public class RegisterViewModel
    {
        public string ID { get; set; }

        [Required(ErrorMessage = "Chưa nhập họ tên.")]
        [MaxLength(256, ErrorMessage = "Họ tên không quá 256 ký tự.")]
        public string FullName { get; set; }
        
        [Required(ErrorMessage = "Chưa nhập tên đăng nhập.")]
        public string UserName { get; set; }
        
        public DateTime? BirthDay { get; set; }
        
        [MinLength(6, ErrorMessage = "Mật khẩu tối thiểu 6 ký tự.")]
        public string Password { get; set; }
                                 
        [Required(ErrorMessage = "Chưa nhập Email.")]
        [EmailAddress(ErrorMessage = "Địa chỉ email chưa đúng.")]
        public string Email { get; set; }
        
        [MaxLength(256, ErrorMessage = "Địa chỉ tối đa 256 ký tự.")]
        public string Address { get; set; }
                
        public string PhoneNumber { get; set; }

        public string Groups = Common.ConfigHelper.GetByKey("CustomerGroup");
        
    }
}