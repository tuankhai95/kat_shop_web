﻿using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{
    public class TagViewModel
    {
        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [MaxLength(50, ErrorMessage =Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_50)]
        public string ID { set; get; }

        [Required(ErrorMessage =Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [MaxLength(50, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_50)]
        public string Name { set; get; }
         
    }
}