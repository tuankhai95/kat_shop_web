﻿using System;

namespace KAT_Shop.Web.Models
{
    [Serializable]
    public class ProductAttributeValueViewModel
    {
                                                          
        public int ID { set; get; }

        public int AttributeId { get; set; }

        public int ProductId { get; set; }

        public string Value { get; set; }
                                             
        public virtual ProductAttributeViewModel Attribute { get; set; }
                                        
        public virtual ProductViewModel Product { get; set; }
    }
}