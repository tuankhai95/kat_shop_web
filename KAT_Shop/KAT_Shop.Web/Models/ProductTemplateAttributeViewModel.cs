﻿namespace KAT_Shop.Web.Models
{
    public class ProductTemplateAttributeViewModel
    {   
        public int ID { get; set; }       

        public int ProductTemplateId { get; set; }
                      
        public int ProductAttributeId { get; set; }
                        
        public virtual ProductTemplateViewModel ProductTemplate { get; set; }
                                                   
        public virtual ProductAttributeViewModel ProductAttributes { get; set; }

    }
}