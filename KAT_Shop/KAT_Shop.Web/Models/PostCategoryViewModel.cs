﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{
    public class PostCategoryViewModel
    {
        public int ID { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [StringLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Name { set; get; }

        [Required(ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_FIELD_NOT_NULL)]
        [StringLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Alias { set; get; }
                                  
        public string Description { set; get; }

        public int? ParentID { set; get; }
        
        public int? DisplayOrder { set; get; }

        [StringLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Image { set; get; }
        
        public bool? HomeFlag { set; get; }

        public virtual IEnumerable<PostViewModel> Posts { set; get; }

        public DateTime? CreatedDate { set; get; }

        public string CreatedBy { set; get; }

        public DateTime? UpdatedDate { set; get; }

        public string UpdatedBy { set; get; }
        
        public string MetaKeyword { set; get; }
                                           
        public string MetaDescription { set; get; }
                                                            
        public bool IsPublished { set; get; }
                 
        public string NormalizedName { get; set; }
    }
}