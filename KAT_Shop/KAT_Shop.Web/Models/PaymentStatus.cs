﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KAT_Shop.Web.Models
{
    public enum PaymentStatus
    {
        Unpaid = 11,
        Paid = 21
    }
}