﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Web.Models
{
    public class ProductAttributeViewModel
    {
        public int ID { set; get; }

        [MaxLength(256, ErrorMessage = Common.CommonNotification.ERROR_MESSAGE_MAX_LENGTH_256)]
        public string Name { get; set; }

        public int ProductAttributeGroupId { get; set; }
                                                      
        public virtual ProductAttributeGroupViewModel ProductAttributeGroup { get; set; }

        public virtual IEnumerable<ProductTemplateAttributeViewModel> ProductTemplates { get; set; }

        public virtual IEnumerable<ProductAttributeValueViewModel> ProductAttributeValues { get; set; }
    }
}