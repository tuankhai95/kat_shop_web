﻿using KAT_Shop.Model.Models;
using KAT_Shop.Web.Models;
using System;

namespace KAT_Shop.Web.Infrastruture.Extensions
{
    public static class EntityExtensions
    {
        public static void UpdateApplicationGroup(this Group appGroup, GroupViewModel appGroupViewModel)
        {
            appGroup.ID = appGroupViewModel.ID;
            appGroup.Name = appGroupViewModel.Name;
            appGroup.Description = appGroupViewModel.Description;
        }

        public static void UpdateApplicationRole(this Role appRole, RoleViewModel appRoleViewModel, string action = "add")
        {
            if (action == "update")
                appRole.Id = appRoleViewModel.Id;
            else
                appRole.Id = Guid.NewGuid().ToString();
            appRole.Name = appRoleViewModel.Name;
            appRole.Description = appRoleViewModel.Description;
        }

        public static void UpdateUser(this User appUser, UserViewModel appUserViewModel, string action = "add")
        {
            appUser.Id = appUserViewModel.Id;
            appUser.FullName = appUserViewModel.FullName;
            appUser.BirthDay = appUserViewModel.BirthDay;
            appUser.Email = appUserViewModel.Email;
            appUser.UserName = appUserViewModel.UserName;
            appUser.PhoneNumber = appUserViewModel.PhoneNumber;
            appUser.Address = appUserViewModel.Address;
            appUser.IsDelete = appUserViewModel.IsDelete;
        }

        public static void UpdatePostCategory(this PostCategory postCategpry, PostCategoryViewModel postCategoryVM)
        {
            postCategpry.ID = postCategoryVM.ID;
            postCategpry.Name = postCategoryVM.Name;
            postCategpry.Alias = postCategoryVM.Alias;
            postCategpry.Description = postCategoryVM.Description;
            postCategpry.ParentID = postCategoryVM.ParentID;
            postCategpry.DisplayOrder = postCategoryVM.DisplayOrder;
            postCategpry.Image = postCategoryVM.Image;
            postCategpry.HomeFlag = postCategoryVM.HomeFlag;
            postCategpry.CreatedBy = postCategoryVM.CreatedBy;
            postCategpry.CreatedDate = postCategoryVM.CreatedDate;
            postCategpry.UpdatedBy = postCategoryVM.UpdatedBy;
            postCategpry.UpdatedDate = postCategoryVM.UpdatedDate;
            postCategpry.MetaKeyword = postCategoryVM.MetaKeyword;
            postCategpry.MetaDescription = postCategoryVM.MetaDescription;
            postCategpry.IsPublished = postCategoryVM.IsPublished;
            postCategpry.NormalizedName = postCategoryVM.NormalizedName;

        }

        public static void UpdatePost(this Post post, PostViewModel postVM)
        {
            post.ID = postVM.ID;
            post.Name = postVM.Name;
            post.Alias = postVM.Alias;
            post.Description = postVM.Description;
            post.CategoryID = postVM.CategoryID;
            post.Content = postVM.Content;
            post.Image = postVM.Image;
            post.HomeFlag = postVM.HomeFlag;
            post.ViewCount = postVM.ViewCount;
            post.CreatedBy = postVM.CreatedBy;
            post.CreatedDate = postVM.CreatedDate;
            post.UpdatedBy = postVM.UpdatedBy;
            post.UpdatedDate = postVM.UpdatedDate;
            post.MetaKeyword = postVM.MetaKeyword;
            post.MetaDescription = postVM.MetaDescription;
            post.IsPublished = postVM.IsPublished;
            post.NormalizedName = postVM.NormalizedName;
        }
        
        public static void UpdateProductCategory(this ProductCategory productCategpry, ProductCategoryViewModel productCategoryVM)
        {
            productCategpry.ID = productCategoryVM.ID;
            productCategpry.Name = productCategoryVM.Name;
            productCategpry.Alias = productCategoryVM.Alias;
            productCategpry.Description = productCategoryVM.Description;
            productCategpry.ParentID = productCategoryVM.ParentID;
            productCategpry.DisplayOrder = productCategoryVM.DisplayOrder;
            productCategpry.Image = productCategoryVM.Image;
            productCategpry.HomeFlag = productCategoryVM.HomeFlag;
            productCategpry.CreatedBy = productCategoryVM.CreatedBy;
            productCategpry.CreatedDate = productCategoryVM.CreatedDate;
            productCategpry.UpdatedBy = productCategoryVM.UpdatedBy;
            productCategpry.UpdatedDate = productCategoryVM.UpdatedDate;
            productCategpry.MetaKeyword = productCategoryVM.MetaKeyword;
            productCategpry.MetaDescription = productCategoryVM.MetaDescription;
            productCategpry.IsPublished = productCategoryVM.IsPublished;
            productCategpry.IsDelete = productCategoryVM.IsDelete;
        }

        public static void UpdateProduct(this Product product, ProductViewModel productVm)
        {
            product.ID = productVm.ID;
            product.Name = productVm.Name;
            product.Description = productVm.Description;
            product.Alias = productVm.Alias;
            product.CategoryID = productVm.CategoryID;
            product.Content = productVm.Content;
            product.Image = productVm.Image;
            product.MoreImages = productVm.MoreImages;
            product.Price = productVm.Price;
            product.PromotionPrice = productVm.PromotionPrice;
            product.Warranty = productVm.Warranty;
            product.HomeFlag = productVm.HomeFlag;
            product.ViewCount = productVm.ViewCount;
            product.HotFlag = productVm.HotFlag;
            product.Quantity = productVm.Quantity;
            product.OriginalPrice = productVm.OriginalPrice;
            product.CreatedDate = productVm.CreatedDate;
            product.CreatedBy = productVm.CreatedBy;
            product.UpdatedDate = productVm.UpdatedDate;
            product.UpdatedBy = productVm.UpdatedBy;
            product.MetaKeyword = productVm.MetaKeyword;
            product.MetaDescription = productVm.MetaDescription;
            product.IsDelete = productVm.IsDelete;
            product.IsPublished = productVm.IsPublished;
            product.NormalizedName = productVm.NormalizedName;
            product.Tags = productVm.Tags;
            product.BrandID = productVm.BrandID;
        }

        public static void UpdateFeedback(this Feedback feedback, FeedbackViewModel feedbackVm)
        {
            feedback.Name = feedbackVm.Name;
            feedback.Email = feedbackVm.Email;
            feedback.Message = feedbackVm.Message;
            feedback.CreatedDate = feedbackVm.CreatedDate;
            feedback.RepliedBy = feedbackVm.RepliedBy;
            feedback.RepliedContent = feedbackVm.RepliedContent;
            feedback.RepliedDate = feedbackVm.RepliedDate;
            feedback.Status = feedbackVm.Status;
        }

        public static void UpdateOrder(this Order order, OrderViewModel orderVm)
        {
            order.CreatedBy = orderVm.CreatedBy;
            order.CreatedDate = orderVm.CreatedDate;
            order.CustomerBillingAddress = orderVm.CustomerBillingAddress;
            order.CustomerEmail = orderVm.CustomerEmail;
            order.CustomerId = orderVm.CustomerId;
            order.CustomerMessage = orderVm.CustomerMessage;
            order.CustomerMobile = orderVm.CustomerMobile;
            order.CustomerName = orderVm.CustomerName;
            order.CustomerShippingAddress = orderVm.CustomerShippingAddress;
            order.ID = orderVm.ID;
            order.PaymentMethod = orderVm.PaymentMethod;
            order.PaymentStatus = orderVm.PaymentStatus;
            order.Status = orderVm.Status;
            order.SubTotal = orderVm.SubTotal;
        }

        public static void UpdateOrderCart(this Order order, ShoppingCartViewModel shopCartVm)              
        {
            order.CreatedBy = shopCartVm.CreatedBy;
            order.CreatedDate = shopCartVm.CreatedDate;
            order.CustomerBillingAddress = shopCartVm.CustomerBillingAddress;
            order.CustomerEmail = shopCartVm.CustomerEmail;
            order.CustomerId = shopCartVm.CustomerId;
            order.CustomerMessage = shopCartVm.CustomerMessage;
            order.CustomerMobile = shopCartVm.CustomerMobile;
            order.CustomerName = shopCartVm.CustomerName;
            order.CustomerShippingAddress = shopCartVm.CustomerShippingAddress;            
            order.PaymentMethod = shopCartVm.PaymentMethod;
            order.SubTotal = shopCartVm.SubTotal;
        }

        public static void UpdateOrderDetail(this OrderDetail orderDetail, OrderDetailViewModel orderDetailVm)
        {
            orderDetail.OrderID = orderDetailVm.OrderID;
            orderDetail.Price = orderDetailVm.Price;
            orderDetail.ProductID = orderDetailVm.ProductID;
            orderDetail.OriginalPrice = orderDetailVm.OriginalPrice;
            orderDetail.Quantity = orderDetailVm.Quantity;
        }

        public static void UpdateProductBrand(this ProductBrand productBrand, ProductBrandViewModel productBrandVm)
        {
            productBrand.Alias = productBrandVm.Alias;
            productBrand.CreatedBy = productBrandVm.CreatedBy;
            productBrand.CreatedDate = productBrandVm.CreatedDate;
            productBrand.Description = productBrandVm.Description;
            productBrand.HomeFlag = productBrandVm.HomeFlag;
            productBrand.ID = productBrandVm.ID;
            productBrand.Image = productBrandVm.Image;
            productBrand.IsDelete = productBrandVm.IsDelete;
            productBrand.IsPublished = productBrandVm.IsPublished;
            productBrand.MetaDescription = productBrandVm.MetaDescription;
            productBrand.MetaKeyword = productBrandVm.MetaKeyword;
            productBrand.Name = productBrandVm.Name;
            productBrand.UpdatedBy = productBrandVm.UpdatedBy;
            productBrand.UpdatedDate = productBrandVm.UpdatedDate;
        }

        public static void UpdateProductAttributeGroup(this ProductAttributeGroup productAttributeGroup, ProductAttributeGroupViewModel productAttributeGroupVm)
        {
            productAttributeGroup.ID = productAttributeGroupVm.ID;
            productAttributeGroup.Name = productAttributeGroupVm.Name;
        }

        public static void UpdateProductAttribute(this ProductAttribute productAttribute, ProductAttributeViewModel productAttributeVm)
        {
            productAttribute.ID = productAttributeVm.ID;
            productAttribute.Name = productAttributeVm.Name;
            productAttribute.ProductAttributeGroupId = productAttributeVm.ProductAttributeGroupId;
        }

        public static void UpdateProductAttributeValue(this ProductAttributeValue productAttributeValue, ProductAttributeValueViewModel productAttributeValueVm)
        {
            productAttributeValue.ID = productAttributeValueVm.ID;
            productAttributeValue.ProductId = productAttributeValueVm.ProductId;
            productAttributeValue.Value = productAttributeValueVm.Value;
            productAttributeValue.AttributeId = productAttributeValueVm.AttributeId;
        }

        public static void UpdateProductTemplate(this ProductTemplate productTemplate, ProductTemplateViewModel productTemplateVm)
        {
            productTemplate.ID = productTemplateVm.ID;
            productTemplate.Name = productTemplateVm.Name;
        }

        public static void UpdateProductTemplateAttribute(this ProductTemplateAttribute productTemplateAttribute, ProductTemplateAttributeViewModel productTemplateAttributeVm)
        {
            productTemplateAttribute.ProductAttributeId = productTemplateAttributeVm.ProductAttributeId;
            productTemplateAttribute.ProductTemplateId = productTemplateAttributeVm.ProductTemplateId;
        }
    }
}