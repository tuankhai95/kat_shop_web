﻿using AutoMapper;
using KAT_Shop.Model.Models;
using KAT_Shop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KAT_Shop.Web.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.CreateMap<Error, ErrorViewModel>();
            Mapper.CreateMap<Feedback, FeedbackViewModel>();
            Mapper.CreateMap<Order, OrderViewModel>();
            Mapper.CreateMap<OrderDetail, OrderDetailViewModel>();
            Mapper.CreateMap<Post, PostViewModel>();
            Mapper.CreateMap<PostCategory, PostCategoryViewModel>();
            Mapper.CreateMap<Tag, TagViewModel>();
            Mapper.CreateMap<Product, ProductViewModel>();
            Mapper.CreateMap<ProductCategory, ProductCategoryViewModel>();
            Mapper.CreateMap<ProductTag, ProductTagViewModel>();
            Mapper.CreateMap<ProductAttribute, ProductAttributeViewModel>();
            Mapper.CreateMap<ProductAttributeGroup, ProductAttributeGroupViewModel>();
            Mapper.CreateMap<ProductAttributeValue, ProductAttributeValueViewModel>();
            Mapper.CreateMap<ProductBrand, ProductBrandViewModel>();
            Mapper.CreateMap<ProductTemplate, ProductTemplateViewModel>();
            Mapper.CreateMap<ProductTemplateAttribute, ProductTemplateAttributeViewModel>();
            Mapper.CreateMap<SystemConfig, SystemConfigViewModel>();
            Mapper.CreateMap<Product, ProductDetailViewModel>();
            Mapper.CreateMap<Order, OrderShowViewModel>();
            Mapper.CreateMap<OrderDetail, OrderDetailShowViewModel>();
            
            Mapper.CreateMap<Group, GroupViewModel>();
            Mapper.CreateMap<Role, RoleViewModel>();
            Mapper.CreateMap<User, UserViewModel>();
        }
    }
}