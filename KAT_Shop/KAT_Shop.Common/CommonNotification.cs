﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAT_Shop.Common
{
    public class CommonNotification
    {      
        public const string ERROR_MESSAGE_FIELD_NOT_NULL = "Trường yêu cầu bắt buộc.";
        public const string ERROR_MESSAGE_MAX_LENGTH_256 = "Độ dài trường tối đa 256 ký tự.";
        public const string ERROR_MESSAGE_MAX_LENGTH_128 = "Độ dài trường tối đa 128 ký tự.";
        public const string ERROR_MESSAGE_MAX_LENGTH_50 = "Độ dài trường tối đa 50 ký tự.";
        public const string ERROR_MESSAGE_MAX_LENGTH_500 = "Độ dài trường tối đa 500 ký tự.";
        public const string ERROR_MESSAGE_MIN_LENGTH_6 = "Độ dài tối thiểu 6 ký tự.";
        public const string ERROR_MESSAGE_FORMAT_EMAIL = "Địa chỉ Email không đúng.";
        public const string ERROR_MESSAGE_FORMAT_PHONE = "Số điện thoại không đúng.";
    }
}
