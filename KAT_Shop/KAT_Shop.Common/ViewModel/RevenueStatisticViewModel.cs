﻿using System;

namespace KAT_Shop.Common.ViewModel
{
    public class RevenueStatisticViewModel
    {
        public DateTime Date { get; set; }

        public decimal Revenues { get; set; }

        public decimal Benefit { get; set; }
    }

    public class ProductStatisticViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Quantities { get; set; }
        public decimal PriceAvg { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal Benefit { get; set; }
    }

    #region list sort
    public enum DefaultListSort
    {
        Poorly_Selling_Product = 0,
        More_Selling_Product = 1,
        Benefit_Low = 2,
        Benefit_High = 3
    }
    #endregion list sort
}
