﻿using System.Configuration;

namespace KAT_Shop.Common
{
    public class ConfigHelper
    {
        public static string GetByKey(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }   
              
    }
}
