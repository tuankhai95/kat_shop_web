﻿using KAT_Shop.Model.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAT_Shop.Data
{
    public class KAT_ShopDbContext   :IdentityDbContext<User>
    {
        public KAT_ShopDbContext() : base("KATShopConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
                      
        public DbSet<Order> Orders { set; get; }
        public DbSet<OrderDetail> OrderDetails { set; get; }       
        public DbSet<Post> Posts { set; get; }
        public DbSet<PostCategory> PostCategories { set; get; }  
        public DbSet<Product> Products { set; get; }      
        public DbSet<ProductCategory> ProductCategories { set; get; }
        public DbSet<ProductTag> ProductTags { set; get; } 
        public DbSet<SystemConfig> SystemConfigs { set; get; }  
        public DbSet<Tag> Tags { set; get; }           
        public DbSet<Error> Errors { get; set; }                    
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<ProductAttribute> ProductAttributes { set; get; }
        public DbSet<ProductAttributeGroup> ProductAttributeGroups { set; get; }
        public DbSet<ProductAttributeValue> ProductAttributeValues { set; get; }
        public DbSet<ProductBrand> ProductBrands { set; get; }
        public DbSet<ProductTemplate> ProductTemplates { set; get; }
        public DbSet<ProductTemplateAttribute> ProductTemplateAttributes { set; get; }


        public DbSet<Group> Groups { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RoleGroup> RoleGroups { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }   
        

        public static KAT_ShopDbContext Create()
        {
            return new KAT_ShopDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {   
            builder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId }).ToTable("UserRoles");
            builder.Entity<IdentityUserLogin>().HasKey(i => i.UserId).ToTable("UserLogins");
            builder.Entity<IdentityRole>().ToTable("Roles");
            builder.Entity<IdentityUserClaim>().HasKey(i => i.UserId).ToTable("UserClaims");
        }
    }
}
