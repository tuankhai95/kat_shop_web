﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Data.Repositories
{
    public interface IProductAttributeValueRepository : IRepository<ProductAttributeValue>
    {

    }
    public class ProductAttributeValueRepository : RepositoryBase<ProductAttributeValue>, IProductAttributeValueRepository
    {
        public ProductAttributeValueRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
