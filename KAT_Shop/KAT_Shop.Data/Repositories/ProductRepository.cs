﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity;

namespace KAT_Shop.Data.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> GetListProductByTag(string tagId, int page, int pageSize, out int totalRow);
        Product GetProductByID(int id);
    }
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Product> GetListProductByTag(string tagId, int page, int pageSize, out int totalRow)
        {
            var query = from p in DbContext.Products
                        join pt in DbContext.ProductTags
                        on p.ID equals pt.ProductId
                        where pt.TagId == tagId
                        select p;
            totalRow = query.Count();
            return query.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public Product GetProductByID(int id)
        {
            var query = from pro in DbContext.Products       
                        where pro.ID == id
                        select pro;
            return query.FirstOrDefault(x => x.ID == id);
        }  
    }
}
