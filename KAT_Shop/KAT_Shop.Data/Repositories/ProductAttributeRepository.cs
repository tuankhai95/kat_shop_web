﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Data.Entity;

namespace KAT_Shop.Data.Repositories
{

    public interface IProductAttributeRepository : IRepository<ProductAttribute>
    {
        IEnumerable<ProductAttribute> GetListAttributeByTemplateID(int id);
        IEnumerable<ProductAttributeValue> GetListAttributeByProductID(int id);
    }
    public class ProductAttributeRepository : RepositoryBase<ProductAttribute>, IProductAttributeRepository
    {
        public ProductAttributeRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<ProductAttributeValue> GetListAttributeByProductID(int id)
        {
            var query = from pav in DbContext.ProductAttributeValues                       
                        where pav.ProductId == id
                        select pav;
            return query.Include(x=>x.Attribute);
        }

        public IEnumerable<ProductAttribute> GetListAttributeByTemplateID(int id)
        {
            var query = from pa in DbContext.ProductAttributes
                        join pta in DbContext.ProductTemplateAttributes
                        on pa.ID equals pta.ProductAttributeId
                        where pta.ProductTemplateId == id
                        select pa;
            return query;
        }
    }
}
