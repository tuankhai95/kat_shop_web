﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Data.Repositories
{
    public interface IErrorRepository : IRepository<Error>
    {   
    }
    public class ErrorRepository : RepositoryBase<Error>, IErrorRepository
    {
        public ErrorRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
