﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Data.Repositories
{
    public interface IProductTemplateRepository : IRepository<ProductTemplate>
    {

    }
    public class ProductTemplateRepository : RepositoryBase<ProductTemplate>, IProductTemplateRepository
    {
        public ProductTemplateRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }

}
