﻿using KAT_Shop.Common.ViewModel;
using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;

namespace KAT_Shop.Data.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        IEnumerable<RevenueStatisticViewModel> GetRevenueStatistic(string fromDate, string toDate);
        IEnumerable<ProductStatisticViewModel> GetProductStatistic(string fromDate, string toDate);
    }

    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<ProductStatisticViewModel> GetProductStatistic(string fromDate, string toDate)
        {
            var parameters = new SqlParameter[]{
                new SqlParameter("@fromDate", fromDate) ,
                    new SqlParameter("@toDate", toDate)
            };
            return DbContext.Database.SqlQuery<ProductStatisticViewModel>("GetProductStatistics @fromDate, @toDate", parameters);
        }

        public IEnumerable<RevenueStatisticViewModel> GetRevenueStatistic(string fromDate, string toDate)
        {
            var parameters = new SqlParameter[]{
                new SqlParameter("@fromDate", fromDate) ,
                    new SqlParameter("@toDate", toDate)
            };
            return DbContext.Database.SqlQuery<RevenueStatisticViewModel>("GetRevenueStatistic @fromDate, @toDate", parameters);
        }
    }
}
