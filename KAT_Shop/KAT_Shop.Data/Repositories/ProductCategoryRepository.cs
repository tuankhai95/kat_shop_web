﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;
using System.Collections.Generic;
using System.Linq;

namespace KAT_Shop.Data.Repositories
{
    public interface IProductCategoryRepository : IRepository<ProductCategory>
    {
        IEnumerable<ProductCategory> GetByAlias(string alias);
    }

    public class ProductCategoryRepository : RepositoryBase<ProductCategory>, IProductCategoryRepository
    {
        public ProductCategoryRepository(IDbFactory dbFactory): base(dbFactory)
        {
        }
        

        public IEnumerable<ProductCategory> GetByAlias(string alias)
        {
            return this.DbContext.ProductCategories.Where(x => !x.IsDelete && x.Alias == alias);
        }
    }
}
