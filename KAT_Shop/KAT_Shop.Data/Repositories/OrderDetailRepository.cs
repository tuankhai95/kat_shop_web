﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace KAT_Shop.Data.Repositories
{
    public interface IOrderDetailRepository : IRepository<OrderDetail>
    {
        IEnumerable<OrderDetail> GetDetailByOrderId(int id);
    }

    public class OrderDetailRepository : RepositoryBase<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<OrderDetail> GetDetailByOrderId(int id)
        {
            var query = from o in DbContext.OrderDetails
                        where o.OrderID == id
                        select o;
            return query.Include(a=>a.Product).OrderBy(x => x.ProductID);
        }
    }
}
