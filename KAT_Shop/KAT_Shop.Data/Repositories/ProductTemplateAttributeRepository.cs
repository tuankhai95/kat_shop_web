﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Data.Repositories
{
    public interface IProductTemplateAttributeRepository : IRepository<ProductTemplateAttribute>
    {                      
    }
    public class ProductTemplateAttributeRepository : RepositoryBase<ProductTemplateAttribute>, IProductTemplateAttributeRepository
    {
        public ProductTemplateAttributeRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

    }
}
