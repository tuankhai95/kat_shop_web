﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Data.Repositories
{
    public interface IRoleGroupRepository : IRepository<RoleGroup>
    {

    }
    public class RoleGroupRepository : RepositoryBase<RoleGroup>, IRoleGroupRepository
    {
        public RoleGroupRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
