﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Data.Repositories
{
    public interface IProductAttributeGroupRepository : IRepository<ProductAttributeGroup>
    {

    }
    public class ProductAttributeGroupRepository : RepositoryBase<ProductAttributeGroup>, IProductAttributeGroupRepository
    {
        public ProductAttributeGroupRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
