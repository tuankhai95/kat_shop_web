﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Data.Repositories
{
    public interface IProductBrandRepository : IRepository<ProductBrand>
    {

    } 
    public class ProductBrandRepository  : RepositoryBase<ProductBrand> , IProductBrandRepository
    {
        public ProductBrandRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
