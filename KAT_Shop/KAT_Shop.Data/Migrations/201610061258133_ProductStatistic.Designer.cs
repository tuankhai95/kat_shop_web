// <auto-generated />
namespace KAT_Shop.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ProductStatistic : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ProductStatistic));
        
        string IMigrationMetadata.Id
        {
            get { return "201610061258133_ProductStatistic"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
