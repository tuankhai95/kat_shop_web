namespace KAT_Shop.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductStatistic : DbMigration
    {
        public override void Up()
        {

            CreateStoredProcedure("GetProductStatistics",
                p => new
                {
                    formDataStatistic = p.String(),
                    toDateStatistic = p.String()
                }
                ,
                @"
                    SELECT  p.ID, p.Name, SUM(od.Quantity) as Quantities, AVG(od.Price) as PriceAvg, AVG(od.OriginalPrice) as OriginalPrice, AVG(od.Price)*SUM(od.Quantity) - AVG(od.OriginalPrice)*SUM(od.Quantity) as Benefit
                    FROM Orders o INNER JOIN OrderDetails od
		            ON o.ID= od.OrderID INNER JOIN Products p
		            ON od.ProductID = p.ID
                    WHERE o.CreatedDate <= CAST(@toDateStatistic as date) and o.CreatedDate >= CAST(@formDataStatistic as date)
	                Group by p.ID, p.Name
	                order by Quantities desc  
                "
                );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.GetProductStatistics");
        }
    }
}
