namespace KAT_Shop.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class RevenueStatistic : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure("GetRevenueStatistic",
                p => new
                {
                    fromDate = p.String(),
                    toDate = p.String()
                }
                ,
                @"
                    SELECT o.CreatedDate as Date,
                    SUM(od.Quantity*od.Price) as Revenues, 
                    SUM((od.Quantity*od.Price)-(od.Quantity*od.OriginalPrice)) as Benefit
                    FROM Orders o INNER JOIN OrderDetails od
                    ON o.ID= od.OrderID INNER JOIN Products p
                    ON od.ProductID = p.ID
                    WHERE o.CreatedDate <= CAST(@toDate as date) and o.CreatedDate >= CAST(@fromDate as date)
                    GROUP BY o.CreatedDate
                    "
                );
        }

        public override void Down()
        {
            DropStoredProcedure("dbo.GetRevenueStatistic");    
        }
    }
}
