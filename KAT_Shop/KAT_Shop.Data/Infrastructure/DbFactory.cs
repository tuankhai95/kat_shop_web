﻿
namespace KAT_Shop.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private KAT_ShopDbContext dbContext;

        public KAT_ShopDbContext Init()
        {
            return dbContext ?? (dbContext = new KAT_ShopDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
