﻿using System;

namespace KAT_Shop.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        KAT_ShopDbContext Init();
    }
}
