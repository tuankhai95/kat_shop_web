﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System.Collections.Generic;

namespace KAT_Shop.Service
{
    public interface IProductBrandService
    {
        ProductBrand Add(ProductBrand productBrand);

        void Update(ProductBrand productBrand);

        ProductBrand Delete(int id);

        IEnumerable<ProductBrand> GetAll();

        IEnumerable<ProductBrand> GetAll(string keyword);           

        ProductBrand GetById(int id);
        void Save();
    }

    public class ProductBrandService : IProductBrandService
    {
        private IProductBrandRepository _productBrandRepository;
        private IUnitOfWork _unitOfWork;

        public ProductBrandService(IProductBrandRepository productBrandRepository, IUnitOfWork unitOfWork)
        {
            this._productBrandRepository = productBrandRepository;
            this._unitOfWork = unitOfWork;
        }

        public ProductBrand Add(ProductBrand productBrand)
        {
            return _productBrandRepository.Add(productBrand);
        }

        public ProductBrand Delete(int id)
        {
            var oldProductBrand = _productBrandRepository.GetSingleById(id);
            oldProductBrand.IsDelete = true;
            oldProductBrand.IsPublished = false;
            _productBrandRepository.Update(oldProductBrand);
            return oldProductBrand;
        }

        public IEnumerable<ProductBrand> GetAll()
        {
            return _productBrandRepository.GetMulti(x=>!x.IsDelete);
        }

        public IEnumerable<ProductBrand> GetAll(string keywork)
        {
            if (!string.IsNullOrEmpty(keywork))
            {
                return _productBrandRepository.GetMulti(x => !x.IsDelete && x.Name.ToUpper().Contains(keywork.ToUpper()));
            }
            else
            {
                return _productBrandRepository.GetMulti(x => !x.IsDelete);
            }
        }

        public ProductBrand GetById(int id)
        {
            return _productBrandRepository.GetSingleById(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ProductBrand productBrand)
        {
            _productBrandRepository.Update(productBrand);
        }
    }
}
