﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Service
{
    public interface IErrorService
    {
        Error Create(Error error);
        void Save();
    }
    public class ErrorService : IErrorService
    {
        IErrorRepository _errorRepository;
        IUnitOfWork _unitOfWork;
        public ErrorService(IErrorRepository errorRepository, IUnitOfWork unitOfwork)
        {
            this._errorRepository = errorRepository;
            this._unitOfWork = unitOfwork;
        }

        public Error Create(Error error)
        {
            return _errorRepository.Add(error);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
