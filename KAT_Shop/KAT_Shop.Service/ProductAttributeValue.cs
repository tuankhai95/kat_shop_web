﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAT_Shop.Service
{
    public interface IProductAttributeValueService
    {
        void AddAttributeValue(IEnumerable<ProductAttributeValue> productAttributeValues, int productId);

        void Update(ProductAttributeValue productAttributeValue);

        ProductAttributeValue Delete(int id);

        IEnumerable<ProductAttributeValue> GetAll();         

        ProductAttributeValue GetById(int id);

        void Save();

        IEnumerable<ProductAttributeValue> GetListByID(int id);

        int GetListIDByAttrIDProID(ProductAttributeValue productAttributeValue);
    }

    public class ProductAttributeValueService : IProductAttributeValueService
    {
        private IProductAttributeValueRepository _productAttributeValueRepository;
        private IUnitOfWork _unitOfWork;

        public ProductAttributeValueService(IProductAttributeValueRepository productAttributeValueRepository, IUnitOfWork unitOfWork)
        {
            this._productAttributeValueRepository = productAttributeValueRepository;
            this._unitOfWork = unitOfWork;
        }

        public void AddAttributeValue(IEnumerable<ProductAttributeValue> productAttributeValues, int productId)
        {
            foreach (var productAttributeValue in productAttributeValues)
            {
                _productAttributeValueRepository.Add(productAttributeValue);
            }
        }

        public ProductAttributeValue Delete(int id)
        {
            return _productAttributeValueRepository.Delete(id);
        }

        public IEnumerable<ProductAttributeValue> GetAll()
        {
            return _productAttributeValueRepository.GetAll();
        }              
       
        public ProductAttributeValue GetById(int id)
        {
            return _productAttributeValueRepository.GetSingleById(id);
        }

        public IEnumerable<ProductAttributeValue> GetListByID(int id)
        {
            return _productAttributeValueRepository.GetMulti(x => x.ProductId == id);
        }

        public int GetListIDByAttrIDProID(ProductAttributeValue productAttributeValue)
        {
            return _productAttributeValueRepository.GetMulti(x => x.AttributeId == productAttributeValue.AttributeId && x.ProductId == productAttributeValue.ProductId).Select(x => x.ID).FirstOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ProductAttributeValue productAttributeValue)
        {
            _productAttributeValueRepository.Update(productAttributeValue);
        }
    }
}
