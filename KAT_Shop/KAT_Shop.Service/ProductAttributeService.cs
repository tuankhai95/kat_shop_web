﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System.Collections.Generic;
using System;

namespace KAT_Shop.Service
{
    public interface IProductAttributeService
    {
        ProductAttribute Add(ProductAttribute productAttribute);

        void Update(ProductAttribute productAttribute);

        ProductAttribute Delete(int id);

        IEnumerable<ProductAttribute> GetAll();

        IEnumerable<ProductAttribute> GetAll(string keyword);             

        ProductAttribute GetById(int id);

        void Save();

        IEnumerable<ProductAttribute> GetListAttributeByTemplateID(int id);

        IEnumerable<ProductAttributeValue> GetListAttributeByProductID(int id);     
    }

    public class ProductAttributeService : IProductAttributeService
    {
        private IProductAttributeRepository _productAttributeRepository;
        private IUnitOfWork _unitOfWork;

        public ProductAttributeService(IProductAttributeRepository productAttributeRepository, IUnitOfWork unitOfWork)
        {
            this._productAttributeRepository = productAttributeRepository;
            this._unitOfWork = unitOfWork;
        }

        public ProductAttribute Add(ProductAttribute productAttribute)
        {
            return _productAttributeRepository.Add(productAttribute);
        }

        public ProductAttribute Delete(int id)
        {
            return _productAttributeRepository.Delete(id);
        }

        public IEnumerable<ProductAttribute> GetAll()
        {
            return _productAttributeRepository.GetAll(new string[] { "ProductAttributeGroup" });
        }

        public IEnumerable<ProductAttribute> GetAll(string keywork)
        {
            if (!string.IsNullOrEmpty(keywork))
            {
                return _productAttributeRepository.GetMulti(x => x.Name.ToUpper().Contains(keywork.ToUpper()), new string[] { "ProductAttributeGroup" });
            }
            else
            {
                return _productAttributeRepository.GetAll(new string[] { "ProductAttributeGroup" });
            }
        }
              
        public ProductAttribute GetById(int id)
        {
            return _productAttributeRepository.GetSingleById(id);
        }

        public IEnumerable<ProductAttributeValue> GetListAttributeByProductID(int id)
        {
            return _productAttributeRepository.GetListAttributeByProductID(id);
        }

        public IEnumerable<ProductAttribute> GetListAttributeByTemplateID(int id)
        {
            return _productAttributeRepository.GetListAttributeByTemplateID(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ProductAttribute productAttribute)
        {
            _productAttributeRepository.Update(productAttribute);
        }
    }
}
