﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System;
using System.Collections.Generic;

namespace KAT_Shop.Service
{
    public interface IOrderDetailService
    {
        IEnumerable<OrderDetail> GetListByOrderId( int id);
    }
    public class OrderDetailService:IOrderDetailService
    {
        private IOrderDetailRepository _orderDetailRepository;
        private IUnitOfWork _unitOfWork;
        public OrderDetailService(IOrderDetailRepository orderDetailRepository, IUnitOfWork unitOfWork)
        {
            _orderDetailRepository = orderDetailRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<OrderDetail> GetListByOrderId(int id)
        {
            return _orderDetailRepository.GetMulti(x => x.OrderID == id);
        }
    }
}
