﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System.Collections.Generic;
using System;

namespace KAT_Shop.Service
{
    public interface IProductTemplateAttributeService
    {                                                                                        
        void AddAttributeToTemplate(IEnumerable<ProductTemplateAttribute> templateAttributes, int tamplateId);

        void Update(ProductTemplateAttribute productTemplateAttribute);

        ProductTemplateAttribute Delete(ProductTemplateAttribute attribute);

        IEnumerable<ProductTemplateAttribute> GetAll();

        IEnumerable<ProductTemplateAttribute> GetListByTemplateID(int id);
                                                                   
        ProductTemplateAttribute GetById(int id);
        void Save();
    }

    public class ProductTemplateAttributeService : IProductTemplateAttributeService
    {
        private IProductTemplateAttributeRepository _productTemplateAttributeRepository;
        private IUnitOfWork _unitOfWork;

        public ProductTemplateAttributeService(IProductTemplateAttributeRepository productTemplateAttributeRepository, IUnitOfWork unitOfWork)
        {
            this._productTemplateAttributeRepository = productTemplateAttributeRepository;
            this._unitOfWork = unitOfWork;
        }
          
        public void AddAttributeToTemplate(IEnumerable<ProductTemplateAttribute> templateAttributes, int tamplateId)
        {               
            foreach (var templateAttribute in templateAttributes)
            {
                _productTemplateAttributeRepository.Add(templateAttribute);
            }                  
        }

        public ProductTemplateAttribute Delete(ProductTemplateAttribute attribute)
        {
            return _productTemplateAttributeRepository.Delete(attribute);
        }

        public IEnumerable<ProductTemplateAttribute> GetAll()
        {
            return _productTemplateAttributeRepository.GetAll();
        }
           
        public ProductTemplateAttribute GetById(int id)
        {
            return _productTemplateAttributeRepository.GetSingleById(id);
        }

        public IEnumerable<ProductTemplateAttribute> GetListByTemplateID(int id)
        {
            return _productTemplateAttributeRepository.GetMulti(x => x.ProductTemplateId == id);     
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ProductTemplateAttribute productTemplateAttribute)
        {
            _productTemplateAttributeRepository.Update(productTemplateAttribute);
        }
    }

}
