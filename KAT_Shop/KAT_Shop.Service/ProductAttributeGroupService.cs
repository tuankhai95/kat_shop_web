﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAT_Shop.Service
{
    public interface IProductAttributeGroupService
    {
        ProductAttributeGroup Add(ProductAttributeGroup productAttributeGroup);

        void Update(ProductAttributeGroup productAttributeGroup);

        ProductAttributeGroup Delete(int id);

        IEnumerable<ProductAttributeGroup> GetAll();

        IEnumerable<ProductAttributeGroup> GetAll(string keyword);
    
        ProductAttributeGroup GetById(int id);
        void Save();
    }

    public class ProductAttributeGroupService : IProductAttributeGroupService
    {
        private IProductAttributeGroupRepository _productAttributeGroupRepository;
        private IUnitOfWork _unitOfWork;

        public ProductAttributeGroupService(IProductAttributeGroupRepository productAttributeGroupRepository, IUnitOfWork unitOfWork)
        {
            this._productAttributeGroupRepository = productAttributeGroupRepository;
            this._unitOfWork = unitOfWork;
        }

        public ProductAttributeGroup Add(ProductAttributeGroup productAttributeGroup)
        {
            return _productAttributeGroupRepository.Add(productAttributeGroup);
        }

        public ProductAttributeGroup Delete(int id)
        {
            return _productAttributeGroupRepository.Delete(id);
        }

        public IEnumerable<ProductAttributeGroup> GetAll()
        {
            return _productAttributeGroupRepository.GetAll();
        }

        public IEnumerable<ProductAttributeGroup> GetAll(string keywork)
        {
            if (!string.IsNullOrEmpty(keywork))
            {
                return _productAttributeGroupRepository.GetMulti(x => x.Name.ToUpper().Contains(keywork.ToUpper()));
            }
            else
            {
                return _productAttributeGroupRepository.GetAll();
            }
        }
      
        public ProductAttributeGroup GetById(int id)
        {
            return _productAttributeGroupRepository.GetSingleById(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ProductAttributeGroup productAttributeGroup)
        {
            _productAttributeGroupRepository.Update(productAttributeGroup);
        }
    }
}
