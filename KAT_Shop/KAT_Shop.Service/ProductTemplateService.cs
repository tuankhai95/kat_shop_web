﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAT_Shop.Service
{
    public interface IProductTemplateService
    {
        ProductTemplate Add(ProductTemplate productTemplate);
                                 
        void Update(ProductTemplate productTemplate);

        ProductTemplate Delete(int id);

        IEnumerable<ProductTemplate> GetAll();

        IEnumerable<ProductTemplate> GetAll(string keyword);       

        ProductTemplate GetById(int id);
        void Save();
    }

    public class ProductTemplateService : IProductTemplateService
    {
        private IProductTemplateRepository _productTemplateRepository;
        private IUnitOfWork _unitOfWork;

        public ProductTemplateService(IProductTemplateRepository productTemplateRepository, IUnitOfWork unitOfWork)
        {
            this._productTemplateRepository = productTemplateRepository;
            this._unitOfWork = unitOfWork;
        }

        public ProductTemplate Add(ProductTemplate productTemplate)
        {
            return _productTemplateRepository.Add(productTemplate);
        }

        public ProductTemplate Delete(int id)
        {
            return _productTemplateRepository.Delete(id);
        }

        public IEnumerable<ProductTemplate> GetAll()
        {
            return _productTemplateRepository.GetAll();
        }

        public IEnumerable<ProductTemplate> GetAll(string keywork)
        {
            if (!string.IsNullOrEmpty(keywork))
            {
                return _productTemplateRepository.GetMulti(x => x.Name.ToUpper().Contains(keywork.ToUpper()));
            }
            else
            {
                return _productTemplateRepository.GetAll();
            }
        }                                                                        

        public ProductTemplate GetById(int id)
        {
            return _productTemplateRepository.GetSingleById(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ProductTemplate productTemplate)
        {
            _productTemplateRepository.Update(productTemplate);
        }
    }
}
