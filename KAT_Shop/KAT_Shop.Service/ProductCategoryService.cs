﻿using System.Collections.Generic;
using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Service
{
    public interface IProductCategoryService
    {
        ProductCategory Add(ProductCategory productCategory);

        void Update(ProductCategory productCategory);

        ProductCategory Delete(int id);

        IEnumerable<ProductCategory> GetAll();

        IEnumerable<ProductCategory> GetAll(string keyword);

        IEnumerable<ProductCategory> GetAllByParentId(int parentId);

        ProductCategory GetById(int id);
        void Save();
    }

    public class ProductCategoryService : IProductCategoryService
    {
        private IProductCategoryRepository _productCategoryRepository;
        private IUnitOfWork _unitOfWork;

        public ProductCategoryService(IProductCategoryRepository productCategoryRepository, IUnitOfWork unitOfWork)
        {
            this._productCategoryRepository = productCategoryRepository;
            this._unitOfWork = unitOfWork;
        }

        public ProductCategory Add(ProductCategory productCategory)
        {
            return _productCategoryRepository.Add(productCategory);
        }

        public ProductCategory Delete(int id)
        {
            var oldProductCategory = _productCategoryRepository.GetSingleById(id);
            oldProductCategory.IsDelete = true;
            oldProductCategory.IsPublished = false;
            _productCategoryRepository.Update(oldProductCategory);
            var childProductCategories = GetAllByParentId(id);
            foreach(var childProductCategory in childProductCategories)
            {
                Delete(childProductCategory.ID);
            }
            return oldProductCategory;
        }
               
        public IEnumerable<ProductCategory> GetAll()
        {
            return _productCategoryRepository.GetMulti(x=>!x.IsDelete);
        }

        public IEnumerable<ProductCategory> GetAll(string keywork)
        {
            if (!string.IsNullOrEmpty(keywork))
            {
                return _productCategoryRepository.GetMulti(x => !x.IsDelete && x.Name.ToUpper().Contains(keywork.ToUpper()));
            }   
            else
            {
                return _productCategoryRepository.GetMulti(x => !x.IsDelete);
            }
        }

        public IEnumerable<ProductCategory> GetAllByParentId(int parentId)
        {
            return _productCategoryRepository.GetMulti(x => !x.IsDelete && x.ParentID == parentId);
        }

        public ProductCategory GetById(int id)
        {
            return _productCategoryRepository.GetSingleById(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ProductCategory productCategory)
        {
            _productCategoryRepository.Update(productCategory);
        }
    }
}
