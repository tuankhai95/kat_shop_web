﻿using System.Collections.Generic;
using System.Linq;
using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Service
{
    public interface IPostService
    {
        Post Add(Post Post);

        void Update(Post Post);

        Post Delete(int id);

        IEnumerable<Post> GetAll();

        IEnumerable<Post> GetAll(string keyword);

        IEnumerable<Post> GetLastest(int top);

        IEnumerable<Post> GetHotPost(int top);

        IEnumerable<Post> GetListPostByCategoryIdPaging(int categoryId, int page, int pageSize, string sort, out int totalRow);

        IEnumerable<Post> Search(string keyword, int page, int pageSize, string sort, out int totalRow);

        IEnumerable<Post> GetReatedPosts(int id, int top);

        IEnumerable<string> GetListPostByName(string name);

        Post GetById(int id);

        void Save();             

        void IncreaseView(int id);      
    }

    public class PostService : IPostService
    {
        private IPostRepository _postRepository;
        private IUnitOfWork _unitOfWork;
        private ITagRepository _tagRepository;

        public PostService(IPostRepository postRepository, IUnitOfWork unitOfWork, ITagRepository tagRepository)
        {
            this._postRepository = postRepository;
            this._unitOfWork = unitOfWork;
            this._tagRepository = tagRepository;
        }

        public Post Add(Post Post)
        {
            return _postRepository.Add(Post);
        }

        public Post Delete(int id)
        {
            return _postRepository.Delete(id);
        }

        public IEnumerable<Post> GetAll()
        {
            return _postRepository.GetAll(new string[] { "PostCategory" });
        }

        public IEnumerable<Post> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                return _postRepository.GetMulti(x => x.Name.ToUpper().Contains(keyword.ToUpper()) || x.Description.ToUpper().Contains(keyword.ToUpper()), new string[] { "PostCategory" });
            }
            else
            {
                return _postRepository.GetAll(new string[] { "PostCategory" });
            }
        }

        public Post GetById(int id)
        {
            return _postRepository.GetSingleById(id);
        }

        public IEnumerable<Post> GetHotPost(int top)
        {
            return _postRepository.GetMulti(x => x.IsPublished && x.HotFlag == true, new string[] { "PostCategory" }).OrderByDescending(x => x.CreatedDate).Take(top);
        }

        public IEnumerable<Post> GetLastest(int top)
        {
            return _postRepository.GetMulti(x => x.IsPublished, new string[] { "PostCategory" }).OrderByDescending(x => x.CreatedDate).Take(top);
        }

        public IEnumerable<Post> GetListPostByCategoryIdPaging(int categoryId, int page, int pageSize, string sort, out int totalRow)
        {
            var query = _postRepository.GetMulti(x => x.IsPublished && x.PostCategory.IsPublished && (x.PostCategory.ParentID == categoryId || x.PostCategory.ID == categoryId), new string[] { "PostCategory" });

            switch (sort)
            {
                case "popular":
                    query = query.OrderByDescending(x => x.ViewCount);
                    break;
                case "namePost":
                    query = query.OrderBy(x => x.Name);
                    break;
                case "newUp":
                default:
                    query = query.OrderByDescending(x => x.CreatedDate);
                    break;
            }

            totalRow = query.Count();

            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<string> GetListPostByName(string name)
        {
            return _postRepository.GetMulti(x => x.IsPublished && x.Name.ToUpper().Contains(name.ToUpper())).Select(y => y.Name);
        }     

        public IEnumerable<Post> GetReatedPosts(int id, int top)
        {
            var post = _postRepository.GetSingleById(id);
            return _postRepository.GetMulti(x => x.IsPublished && x.ID != id && x.CategoryID == post.CategoryID, new string[] { "PostCategory" }).OrderByDescending(y => y.CreatedDate).Take(top);
        }

      
        public void IncreaseView(int id)
        {
            var post = _postRepository.GetSingleById(id);
            if (post.ViewCount.HasValue)
            {
                post.ViewCount += 1;
            }
            else
            {
                post.ViewCount = 1;
            }
            _postRepository.Update(post);
            _unitOfWork.Commit();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<Post> Search(string keyword, int page, int pageSize, string sort, out int totalRow)
        {
            var query = _postRepository.GetMulti(x => x.IsPublished && x.Name.ToUpper().Contains(keyword.ToUpper()), new string[] { "PostCategory" });

            switch (sort)
            {
                case "popular":
                    query = query.OrderByDescending(x => x.ViewCount);
                    break;
                case "namePost":
                    query = query.OrderBy(x => x.Name);
                    break;
                case "newUp":
                default:
                    query = query.OrderByDescending(x => x.CreatedDate);
                    break;
            }

            totalRow = query.Count();

            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Update(Post post)
        {
            _postRepository.Update(post);
           
        }


    }
}
