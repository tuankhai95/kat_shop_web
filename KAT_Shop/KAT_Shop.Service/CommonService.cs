﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System.Collections.Generic;
using System;

namespace KAT_Shop.Service
{
    public interface ICommonService
    {
        SystemConfig GetSystemConfig(string code);
        IEnumerable<SystemConfig> GetAllSystemConfig();
        void Create(string code, string strValue, int? intValue);
        void SaveChange();
    }
    public class CommonService : ICommonService
    {
        ISystemConfigRepository _systemConfigRepository;
        IUnitOfWork _unitOfWork;
        public CommonService(ISystemConfigRepository systemConfigRepository, IUnitOfWork unitOfWork)
        {
            _systemConfigRepository = systemConfigRepository;
            _unitOfWork = unitOfWork;
        }
        public SystemConfig GetSystemConfig(string code)
        {
            return _systemConfigRepository.GetSingleByCondition(x => x.Code == code);
        }

        public IEnumerable<SystemConfig> GetAllSystemConfig()
        {
            return _systemConfigRepository.GetAll();
        }

        public void Create(string code, string strValue, int? intValue)
        {

            SystemConfig sysConfig = new SystemConfig()
            {
                Code = code,
                ValueString = strValue,
                ValueInt = intValue
            };
            _systemConfigRepository.Add(sysConfig);     

        }

        public void SaveChange()
        {
            _unitOfWork.Commit();
        }
    }
}
