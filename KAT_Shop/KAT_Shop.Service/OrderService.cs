﻿using System;
using System.Collections.Generic;
using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System.Linq;

namespace KAT_Shop.Service
{
    public interface IOrderService
    {
        bool Create(Order order, List<OrderDetail> orderDetails);

        void Update(Order order);

        Order Delete(int id);

        IEnumerable<Order> GetAll();

        IEnumerable<Order> GetAll(string keyword);

        Order GetById(int id);

        Order GetByEmailAndId(string email, int id);

        IEnumerable<Order> GetListByEmail(string email);

        IEnumerable<Order> GetListByCustomerId(string id);

        IEnumerable<OrderDetail> GetListDetailById(int id);

        IEnumerable<Order> GetLatestOrder(int top);

        int GetNumberOrderNew();

        void Save();
    }
    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepository;
        private IOrderDetailRepository _orderDetailRepository;
        private ICommonService _commonService;
        private IUnitOfWork _unitOfWork;

        public OrderService(IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IUnitOfWork unitOfWork, ICommonService commonService)
        {
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _commonService = commonService;
            _unitOfWork = unitOfWork;
        }

        public bool Create(Order order, List<OrderDetail> orderDetails)
        {
            try
            {
                _orderRepository.Add(order);
                _unitOfWork.Commit();        
                foreach (var orderDetail in orderDetails)
                {
                    orderDetail.OrderID = order.ID;
                    _orderDetailRepository.Add(orderDetail);
                }
                return true;
            }
            catch 
            {
                try
                {
                    _orderRepository.Delete(order.ID);
                    _unitOfWork.Commit();
                    return false;
                }
                catch 
                {
                    throw;
                }
            }
        }

        public void Update(Order order)
        {
            _orderRepository.Update(order);
        }

        public Order Delete(int id)
        {
            return _orderRepository.Delete(id);
        }

        public IEnumerable<Order> GetAll()
        {
            return _orderRepository.GetAll();
        }

        public Order GetById(int id)
        {
            return _orderRepository.GetSingleById(id);
        }

        public IEnumerable<Order> GetListByEmail(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                return _orderRepository.GetMulti(x => x.CustomerEmail.ToUpper() == email.ToUpper() || x.User.Email.ToUpper() == email.ToUpper());
            }
            else
            {
                return _orderRepository.GetAll();
            }
        }

        public IEnumerable<Order> GetListByCustomerId(string id)
        {
            return _orderRepository.GetMulti(x => x.User.Id.ToUpper() == id.ToUpper());
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public Order GetByEmailAndId(string email, int id)
        {
            if (!string.IsNullOrEmpty(email))
            {
                return _orderRepository.GetSingleByCondition(x => x.CustomerEmail.ToUpper() == email.ToUpper() && x.ID == id);
            }
            else
            {
                return GetById(id);
            }
        }

        public IEnumerable<Order> GetAll(string keyword)
        {                   
            if (!string.IsNullOrEmpty(keyword))
            {
                return _orderRepository.GetMulti(x => x.CustomerName.ToUpper().Contains(keyword.ToUpper()) || x.CustomerEmail.ToUpper().Contains(keyword.ToUpper()));
            }
            else
            {
                return _orderRepository.GetAll();
            }     
        }

        public IEnumerable<OrderDetail> GetListDetailById(int id)
        {
            return _orderDetailRepository.GetDetailByOrderId(id);
        }

        public int GetNumberOrderNew()
        {
            var value = _commonService.GetSystemConfig("Pending").ValueInt;
            if (value == null)
            {
                return 0;
            }
            return _orderRepository.GetMulti(x => x.Status == value).Count();
        }

        public IEnumerable<Order> GetLatestOrder(int top)
        {
            return _orderRepository.GetAll().OrderByDescending(x => x.CreatedDate).Take(top);
        }
    }
}
