﻿using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;
using System.Collections.Generic;
using System;
using System.Linq;

namespace KAT_Shop.Service
{
    public interface IFeedbackService 
    {
        Feedback Add(Feedback feedback);

        void Update(Feedback feedback);

        Feedback Delete(int id);

        IEnumerable<Feedback> GetAll();
                                                         
        IEnumerable<Feedback> GetAll(string keyword);

        Feedback GetById(int id);

        Feedback Create(Feedback feedback);

        void Reply(Feedback feedback);

        IEnumerable<Feedback> GetLatestFeedback(int top);

        void Save();

        int GetNumberFeedback();
    }
    public class FeedbackService : IFeedbackService
    {
        IFeedbackRepository _feedbackRepository;
        IUnitOfWork _unitOfWork;
        public FeedbackService(IFeedbackRepository feedbackRepository, IUnitOfWork unitOfWork)
        {
            this._feedbackRepository = feedbackRepository;
            this._unitOfWork = unitOfWork;
        }
        public Feedback Create(Feedback feedback)
        {
            return _feedbackRepository.Add(feedback);
        }

        public void Reply(Feedback feedback)
        {
            _feedbackRepository.Update(feedback);
            _unitOfWork.Commit();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
        public IEnumerable<Feedback> GetAll()
        {
            return _feedbackRepository.GetAll();
        }

        public IEnumerable<Feedback> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                return _feedbackRepository.GetMulti(x => x.Email.ToUpper().Contains(keyword.ToUpper()));
            }
            else
            {
                return _feedbackRepository.GetAll();
            }
        }

        public Feedback GetById(int id)
        {
            return _feedbackRepository.GetSingleById(id);
        }

        public Feedback Add(Feedback feedback)
        {
            return _feedbackRepository.Add(feedback);
        }

        public void Update(Feedback feedback)
        {
             _feedbackRepository.Update(feedback);
        }

        public Feedback Delete(int id)
        {
            return _feedbackRepository.Delete(id);
        }

        public int GetNumberFeedback()
        {
            return _feedbackRepository.GetMulti(x => !x.Status).OrderByDescending(x=>x.CreatedDate).Count();
           
        }

        public IEnumerable<Feedback> GetLatestFeedback(int top)
        {
            return _feedbackRepository.GetAll().OrderByDescending(x => x.CreatedDate).Take(top);
        }
    }
}
