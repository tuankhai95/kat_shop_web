﻿using System;
using System.Collections.Generic;
using System.Linq;
using KAT_Shop.Common;
using KAT_Shop.Data.Infrastructure;
using KAT_Shop.Data.Repositories;
using KAT_Shop.Model.Models;

namespace KAT_Shop.Service
{
    public interface IProductService
    {
        Product Add(Product Product);   
        void Update(Product Product);   
        Product Delete(int id);
        Product Remove(int id);
        IEnumerable<Product> GetAll(); 
        IEnumerable<Product> GetAll(string keyword); 
        IEnumerable<Product> GetLastest(int top);
        IEnumerable<Product> GetHotProduct(int top);
        IEnumerable<Product> GetListProductByCategoryId(int categoryId);
        IEnumerable<Product> GetListProductByBrandId(int brandId);
        IEnumerable<Product> GetListProductByCategoryIdPaging(int categoryId, int page, int pageSize,string sort, out int totalRow);       
        IEnumerable<Product> Search(string keyword, int page, int pageSize, string sort, out int totalRow);
        IEnumerable<Product> GetReatedProducts(int id, int top);
        IEnumerable<Product> GetListProductByName(string name);
        Product GetById(int id);
        void Save();           
        Tag GetTag(string tagID);
        IEnumerable<Tag> GetListTagByProductId(int id);   
        void IncreaseView(int id);                    
        IEnumerable<Product> GetListProductByTag(string tagId, int page, int pageSize, out int totalRow);  
        bool SellProduct(int productId, int quantity);

        Product GetProductByID(int id);
    }

    public class ProductService : IProductService
    {
        private IProductRepository _productRepository;
        private ITagRepository _tagRepository;
        private IProductTagRepository _productTagRepository;
        private IUnitOfWork _unitOfWork;

        public ProductService(IProductRepository productRepository, IProductTagRepository productTagRepository, ITagRepository tagRepository, IUnitOfWork unitOfWork)
        {
            this._productRepository = productRepository;
            this._productTagRepository = productTagRepository;
            this._tagRepository = tagRepository;
            this._unitOfWork = unitOfWork;
        }

        public Product Add(Product Product)
        {
            var product = _productRepository.Add(Product);
            _unitOfWork.Commit();
            if (!string.IsNullOrEmpty(Product.Tags))
            {
                string[] tags = Product.Tags.Split(',');
                for (var i = 0; i < tags.Length; i++)
                {
                    var tagId = StringHelper.ToUnsignString(tags[i]);
                    if (_tagRepository.Count(x => x.ID == tagId) == 0)
                    {
                        Tag tag = new Tag();
                        tag.ID = tagId;
                        tag.Name = tags[i];       
                        _tagRepository.Add(tag);
                    }

                    ProductTag productTag = new ProductTag();
                    productTag.ProductId = Product.ID;
                    productTag.TagId = tagId;
                    _productTagRepository.Add(productTag);
                }
                                         
            }
            return product;
        }

        public Product Delete(int id)
        {                                           
            var oldProduct = _productRepository.GetSingleById(id);
            oldProduct.IsDelete = true;
            oldProduct.IsPublished = false;
            _productRepository.Update(oldProduct);
            return oldProduct;
        }

        public IEnumerable<Product> GetAll()
        {
            return _productRepository.GetMulti(x=>!x.IsDelete, new string[] { "ProductCategory", "ProductBrand"});
        }

        public IEnumerable<Product> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                return _productRepository.GetMulti(x => x.Name.ToUpper().Contains(keyword.ToUpper()) && !x.IsDelete, new string[] { "ProductCategory", "ProductBrand" });
            }
            else
            {
                return _productRepository.GetMulti(x => !x.IsDelete, new string[] { "ProductCategory", "ProductBrand" });
            }
        }

        public Product GetById(int id)
        {
            return _productRepository.GetMulti(x=>!x.IsDelete && x.ID == id,new string[] { "ProductCategory", "ProductBrand" }).FirstOrDefault(x=>x.ID==id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(Product product)
        {
            _productRepository.Update(product);
            if (!string.IsNullOrEmpty(product.Tags))
            {
                string[] tags = product.Tags.Split(',');
                for (var i = 0; i < tags.Length; i++)
                {
                    var tagId = StringHelper.ToUnsignString(tags[i]);
                    if (_tagRepository.Count(x => x.ID == tagId) == 0)
                    {
                        Tag tag = new Tag();
                        tag.ID = tagId;
                        tag.Name = tags[i];                     
                        _tagRepository.Add(tag);
                    }

                    _productTagRepository.DeleteMulti(x => x.ProductId == product.ID);
                    ProductTag productTag = new ProductTag();
                    productTag.ProductId = product.ID;
                    productTag.TagId = tagId;
                    _productTagRepository.Add(productTag);
                }
                                         
            }
        }

        public IEnumerable<Product> GetLastest(int top)
        {
            return _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished).OrderByDescending(x => x.CreatedDate).Take(top);
        }

        public IEnumerable<Product> GetHotProduct(int top)
        {
            return _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished && x.HotFlag==true).OrderByDescending(x => x.CreatedDate).Take(top);
        }
        public IEnumerable<Product> GetListProductByCategoryId(int categoryId)
        {
            var query = _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished && x.CategoryID == categoryId).OrderByDescending(x=>x.Name);
                    
            return query;
        }

        public IEnumerable<Product> GetListProductByCategoryIdPaging(int categoryId, int page, int pageSize,string sort, out int totalRow )
        {
            var query = _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished && x.CategoryID == categoryId);

            switch (sort)
            { 
                case "popular":
                    query = query.OrderByDescending(x => x.ViewCount);
                    break;
                case "discount":
                    query = query.OrderByDescending(x => x.PromotionPrice.HasValue);
                    break;
                case "priceAsc":
                    query = query.OrderBy(x => x.Price);
                    break;
                case "priceDsc":
                    query = query.OrderByDescending(x => x.Price);
                    break;
                default:
                    query = query.OrderByDescending(x => x.CreatedDate);
                    break;
            }

            totalRow = query.Count();

            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<Product> GetListProductByName(string name)
        {
            var nameUnsign = Common.StringHelper.ToUnsignStringName(name);
            //var listName = nameUnsign.Split(' ');
            //return _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished).Where(x=>listName.Any(y=>x.NormalizedName.ToUpper().Contains(y.ToUpper())));
            return _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished && x.NormalizedName.ToUpper().Contains(nameUnsign.ToUpper()));
        }

        public IEnumerable<Product> Search(string keyword, int page, int pageSize, string sort, out int totalRow)
        {
            var query = _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished && x.Name.Contains(keyword));

            switch (sort)
            {
                case "popular":
                    query = query.OrderByDescending(x => x.ViewCount);
                    break;
                case "discount":
                    query = query.OrderByDescending(x => x.PromotionPrice.HasValue);
                    break;
                case "priceAsc":
                    query = query.OrderBy(x => x.Price);
                    break;
                case "priceDsc":
                    query = query.OrderByDescending(x => x.Price);
                    break;
                default:
                    query = query.OrderByDescending(x => x.CreatedDate);
                    break;
            }

            totalRow = query.Count();

            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<Product> GetReatedProducts(int id, int top)
        {
            var product = _productRepository.GetSingleById(id);
            return _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished && x.ID != id && x.CategoryID == product.CategoryID).OrderByDescending(y=>y.CreatedDate).Take(top);
        }

        public IEnumerable<Tag> GetListTagByProductId(int id)
        {
            return _productTagRepository.GetMulti(x => x.ProductId == id, new string[] { "Tag" }).Select(y => y.Tag);
        }

        public void IncreaseView(int id)
        {
            var product = _productRepository.GetSingleById(id);
            if (product.ViewCount.HasValue)
            {
                product.ViewCount += 1;
            }
            else
            {
                product.ViewCount = 1;
            }
            _productRepository.Update(product);
            _unitOfWork.Commit();
        }

        public IEnumerable<Product> GetListProductByTag(string tagId, int page, int pageSize, out int totalRow)
        {  
            var model = _productRepository.GetListProductByTag(tagId, page, pageSize, out totalRow);
            return model;
        }

        public Tag GetTag(string tagID)
        {
            return _tagRepository.GetSingleByCondition(x => x.ID == tagID);
        }

        //Selling product
        public bool SellProduct(int productId, int quantity)
        {
            var product = _productRepository.GetSingleById(productId);
            if(product.Quantity < quantity)
            {
                return false;
            }
            product.Quantity -= quantity;
            return true;
        }

        public Product Remove(int id)
        {
           return _productRepository.Delete(id);
        }

        public Product GetProductByID(int id)
        {
            return _productRepository.GetProductByID(id);
        }

        public IEnumerable<Product> GetListProductByBrandId(int brandId)
        {
            var query = _productRepository.GetMulti(x => !x.IsDelete && x.IsPublished && x.BrandID == brandId).OrderByDescending(x => x.Name);

            return query;
        }
    }
}
