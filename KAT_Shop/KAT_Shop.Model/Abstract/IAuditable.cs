﻿using System;

namespace KAT_Shop.Model.Abstract
{
    public interface IAuditable
    {
        DateTime? CreatedDate { set; get; }

        string CreatedBy { set; get; }

        DateTime? UpdatedDate { set; get; }

        string UpdatedBy { set; get; }

        string MetaKeyword { set; get; }

        string MetaDescription { set; get; }

        bool IsPublished { set; get; }
    }
}
