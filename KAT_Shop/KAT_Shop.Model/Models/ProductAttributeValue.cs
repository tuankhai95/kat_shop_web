﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("ProductAttributeValues")]
    public class ProductAttributeValue
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        public int AttributeId { get; set; }
                                            
        public int ProductId { get; set; }         

        public string Value { get; set; }

        [ForeignKey("AttributeId")]
        public virtual ProductAttribute Attribute { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }
}
