﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("UserGroups")]
    public class UserGroup
    {
        [StringLength(128)]
        [Key]
        [Column(Order = 1)]
        public string UserId { set; get; }

        [Key]
        [Column(Order = 2)]
        public int GroupId { set; get; }

        [ForeignKey("UserId")]
        public virtual User User { set; get; }

        [ForeignKey("GroupId")]
        public virtual Group Group { set; get; }
    }
}
