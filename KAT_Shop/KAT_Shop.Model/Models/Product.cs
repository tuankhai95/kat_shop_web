﻿using KAT_Shop.Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("Products")]
    public class Product : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        [Required]
        [MaxLength(256)]
        public string Name { set; get; }

        [Required]
        [MaxLength(256)]
        [Column(TypeName ="varchar")]
        public string Alias { set; get; }

        [Required]
        public int BrandID { get; set; }

        [Required]
        public int CategoryID { set; get; }
                
        [MaxLength(256)]
        [Required]
        public string Image { set; get; }
                     
        [Column(TypeName = "xml")]
        public string MoreImages { set; get; }

        public decimal OriginalPrice { get; set; }

        public decimal Price { set; get; }

        public decimal? PromotionPrice { set; get; }

        public int Quantity { get; set; }

        public int? Warranty { set; get; }

        [MaxLength(500)]
        public string Description { set; get; }

        public string Content { set; get; }      

        public string Tags { get; set; }

        [MaxLength(256)]
        public string NormalizedName { get; set; }

        public bool? HomeFlag { set; get; }

        public bool? HotFlag { set; get; }

        public int? ViewCount { set; get; }

        public bool IsDelete { get; set; }

        [ForeignKey("BrandID")]
        public virtual ProductBrand ProductBrand { get; set; }

        [ForeignKey("CategoryID")]
        public virtual ProductCategory ProductCategory { set; get; }
                                   
        public virtual IEnumerable<ProductTag> ProductTags { get; set; }

        public virtual IEnumerable<ProductAttributeValue> AttributeValues { get; set; }
    }
}
