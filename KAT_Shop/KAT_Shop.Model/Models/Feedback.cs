﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("Feedbacks")]
    public class Feedback
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(256)]
        [Required]
        public string Name { get; set; }

        [StringLength(256)]
        public string Email { get; set; }
                                       
        public string Message { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [StringLength(256)]
        public string RepliedBy { get; set; }

        public DateTime? RepliedDate { get; set; }
        
        public string RepliedContent { get; set; }

        [Required]
        public bool Status { get; set; }
    }
}
