﻿using KAT_Shop.Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("ProductBrands")]         
    public class ProductBrand  : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        [Required]
        [MaxLength(256)]
        public string Name { set; get; }

        [Required]
        [MaxLength(256)]
        public string Alias { set; get; }
        
        public string Description { set; get; }     

        [MaxLength(256)]
        public string Image { set; get; }

        public bool? HomeFlag { get; set; }

        public bool IsDelete { get; set; }      

        public virtual IEnumerable<Product> Products { set; get; }
    }
}
