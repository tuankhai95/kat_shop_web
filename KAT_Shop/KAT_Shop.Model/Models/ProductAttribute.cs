﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("ProductAttributes")]
    public class ProductAttribute
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        [MaxLength(256)]
        public string Name { get; set; }

        public int ProductAttributeGroupId { get; set; }

        [ForeignKey("ProductAttributeGroupId")]
        public virtual ProductAttributeGroup ProductAttributeGroup { get; set; }

        public virtual IEnumerable<ProductTemplateAttribute> ProductTemplates { get; set; }

        public virtual IEnumerable<ProductAttributeValue> ProductAttributeValues { get; set; }
    }
}
