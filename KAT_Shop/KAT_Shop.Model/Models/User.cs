﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KAT_Shop.Model.Models
{
    public class User : IdentityUser
    {
        [StringLength(256)]
        [Required]
        public string FullName { set; get; }

        [StringLength(256)]
        public string Address { set; get; }

        public DateTime? BirthDay { set; get; }
       
        public Boolean IsDelete { get; set; }
        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual IEnumerable<Order> Orders { set; get; }

    }
}
