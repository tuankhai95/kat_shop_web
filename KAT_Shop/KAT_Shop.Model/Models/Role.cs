﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace KAT_Shop.Model.Models
{
    public class Role : IdentityRole
    {
        public Role() : base()
        {

        }
        [StringLength(256)]
        public string Description { set; get; }
    }
}
