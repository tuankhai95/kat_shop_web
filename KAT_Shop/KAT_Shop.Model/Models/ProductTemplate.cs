﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("ProductTemplates")]
    public class ProductTemplate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        public virtual IEnumerable<ProductAttribute> ProductAttributes { get; set; }
        public virtual IEnumerable<ProductTemplateAttribute> ProductTemplateAttributes { get; set; }

    }
}
