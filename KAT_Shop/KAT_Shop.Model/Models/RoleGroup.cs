﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("RoleGroups")]
    public class RoleGroup
    {
        [Key]
        [Column(Order = 1)]
        public int GroupId { set; get; }
                        
        [Key]
        [Column(Order = 2)]
        [StringLength(128)]
        public string RoleId { set; get; }

        [ForeignKey("RoleId")]
        public virtual Role Role { set; get; }

        [ForeignKey("GroupId")]
        public virtual Group Group { set; get; }
    }
}
