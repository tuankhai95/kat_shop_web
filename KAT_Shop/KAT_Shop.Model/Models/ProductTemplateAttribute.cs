﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("ProductTemplateAttributes")]
    public class ProductTemplateAttribute
    {
        [Key]
        [Column(Order = 1)]
        public int ProductTemplateId { get; set; }

        [Column(Order = 2)]    
        [Key]
        public int ProductAttributeId { get; set; }

        [ForeignKey("ProductTemplateId")]
        public virtual ProductTemplate ProductTemplate { get; set; }

        [ForeignKey("ProductAttributeId")]
        public virtual ProductAttribute ProductAttributes { get; set; }
                           
    }
}
