﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KAT_Shop.Model.Models
{
    [Table("Orders")]
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        [Required]
        [MaxLength(256)]
        public string CustomerName { set; get; }

        [Required]
        [MaxLength(256)]
        public string CustomerEmail { set; get; }

        [Required]
        [MaxLength(50)]
        public string CustomerMobile { set; get; }
                      
        [Required]                                     
        public string CustomerShippingAddress { get; set; }
       
        [MaxLength(500)]
        public string CustomerMessage { set; get; }
 
        public DateTime? CreatedDate { set; get; }

        public string CreatedBy { set; get; }

        [MaxLength(256)]
        public string PaymentMethod { set; get; }
               
        public int PaymentStatus { set; get; }
        
        public string CustomerBillingAddress { set; get; }

        [Required]
        public int Status { set; get; }

        [Required]
        public decimal SubTotal { get; set; }

        [StringLength(128)]
        [Column(TypeName = "nvarchar")]
        public string CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual User User { get; set; }

        public virtual IEnumerable<OrderDetail> OrderDetails { set; get; }
    }
}
